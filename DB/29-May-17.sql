USE [MagadhamApp]
GO
/****** Object:  StoredProcedure [dbo].[proc_BusAllotmentDelete]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_BusAllotmentDelete]
(
	@AllotmentId int
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	DELETE
	FROM [BusAllotment]
	WHERE
		[AllotmentId] = @AllotmentId
	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_BusAllotmentInsert]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_BusAllotmentInsert]
(
	@AllotmentId int = NULL output,
	@AllotmentCode nvarchar(50),
	@StudentId int,
	@VehicleId int,
	@DisplayOrder int,
	@SessionId int,
	@CreatedOn datetime
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	INSERT
	INTO [BusAllotment]
	(
		[AllotmentCode],
		[StudentId],
		[VehicleId],
		[DisplayOrder],
		[SessionId],
		[CreatedOn]
	)
	VALUES
	(
		@AllotmentCode,
		@StudentId,
		@VehicleId,
		@DisplayOrder,
		@SessionId,
		@CreatedOn
	)

	SET @Err = @@Error

	SELECT @AllotmentId = SCOPE_IDENTITY()

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_BusAllotmentLoadAll]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_BusAllotmentLoadAll]
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[AllotmentId],
		[AllotmentCode],
		[StudentId],
		[VehicleId],
		[DisplayOrder],
		[SessionId],
		[CreatedOn]
	FROM [BusAllotment]

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_BusAllotmentLoadByPrimaryKey]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_BusAllotmentLoadByPrimaryKey]
(
	@AllotmentId int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[AllotmentId],
		[AllotmentCode],
		[StudentId],
		[VehicleId],
		[DisplayOrder],
		[SessionId],
		[CreatedOn]
	FROM [BusAllotment]
	WHERE
		([AllotmentId] = @AllotmentId)

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_BusAllotmentLoadByStudentIdSessionId]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[proc_BusAllotmentLoadByStudentIdSessionId]
(
	@StudentId int,
	@SessionId int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT *
	FROM [BusAllotment]
	WHERE
		([StudentId] = @StudentId) and ([SessionId] = @SessionId)

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_BusAllotmentLoadByVehicleId]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_BusAllotmentLoadByVehicleId]
(
	@VehicleId int,
	@SessionId int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT *
	FROM [VBusAllotment]
	WHERE
		([VehicleId] = @VehicleId) and ([SessionId] = @SessionId) and IsActive = 'True' order by DisplayOrder ASC

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_BusAllotmentLoadForDrop]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[proc_BusAllotmentLoadForDrop]
(
	@VehicleId int,
	@SessionId int,
	@TodayDate datetime
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	  select * from VBusAllotment S  where
VehicleId = @VehicleId and SessionId = @SessionId and StudentId not in (select StudentId from DropTransaction where CreatedOn = @TodayDate)
  order by DisplayOrder Desc

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_BusAllotmentLoadForPickup]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[proc_BusAllotmentLoadForPickup]
(
	@VehicleId int,
	@SessionId int,
	@TodayDate datetime
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	select * from VBusAllotment S  where
VehicleId = @VehicleId and SessionId = @SessionId and StudentId not in (select StudentId from PickupTransaction where CreatedOn = @TodayDate)
  order by DisplayOrder Asc

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_BusAllotmentUpdate]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_BusAllotmentUpdate]
(
	@AllotmentId int,
	@AllotmentCode nvarchar(50),
	@StudentId int,
	@VehicleId int,
	@DisplayOrder int,
	@SessionId int,
	@CreatedOn datetime
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	UPDATE [BusAllotment]
	SET
		[AllotmentCode] = @AllotmentCode,
		[StudentId] = @StudentId,
		[VehicleId] = @VehicleId,
		[DisplayOrder] = @DisplayOrder,
		[SessionId] = @SessionId,
		[CreatedOn] = @CreatedOn
	WHERE
		[AllotmentId] = @AllotmentId


	SET @Err = @@Error


	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_BusAllotmentUpdateAllotmentCode]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[proc_BusAllotmentUpdateAllotmentCode]
(
	@AllotmentId int,
	@AllotmentCode nvarchar(50)
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	UPDATE [BusAllotment]
	SET
		[AllotmentCode] = @AllotmentCode
	WHERE
		[AllotmentId] = @AllotmentId


	SET @Err = @@Error


	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_BusAllotmentUpdateDisplayOrder]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[proc_BusAllotmentUpdateDisplayOrder]
(
	@AllotmentId int,
	@DisplayOrder int
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	UPDATE [BusAllotment]
	SET
		[DisplayOrder] = @DisplayOrder
	WHERE
		[AllotmentId] = @AllotmentId


	SET @Err = @@Error


	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_ClassMasterDelete]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_ClassMasterDelete]
(
	@ClassId int
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	DELETE
	FROM [ClassMaster]
	WHERE
		[ClassId] = @ClassId
	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_ClassMasterInsert]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_ClassMasterInsert]
(
	@ClassId int = NULL output,
	@ClassName nvarchar(50)
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	INSERT
	INTO [ClassMaster]
	(
		[ClassName]
	)
	VALUES
	(
		@ClassName
	)

	SET @Err = @@Error

	SELECT @ClassId = SCOPE_IDENTITY()

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_ClassMasterLoadAll]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_ClassMasterLoadAll]
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[ClassId],
		[ClassName]
	FROM [ClassMaster]

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_ClassMasterLoadByPrimaryKey]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_ClassMasterLoadByPrimaryKey]
(
	@ClassId int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[ClassId],
		[ClassName]
	FROM [ClassMaster]
	WHERE
		([ClassId] = @ClassId)

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_ClassMasterUpdate]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_ClassMasterUpdate]
(
	@ClassId int,
	@ClassName nvarchar(50)
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	UPDATE [ClassMaster]
	SET
		[ClassName] = @ClassName
	WHERE
		[ClassId] = @ClassId


	SET @Err = @@Error


	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_DriverMasterDelete]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_DriverMasterDelete]
(
	@DriverId int
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	DELETE
	FROM [DriverMaster]
	WHERE
		[DriverId] = @DriverId
	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_DriverMasterInsert]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_DriverMasterInsert]
(
	@DriverId int = NULL output,
	@DriverName nvarchar(50),
	@DriverFatherName nvarchar(50),
	@DriverMobileNo nvarchar(50),
	@DriverAddress nvarchar(MAX),
	@IsActive bit,
	@CreatedOn datetime,
	@UserName nvarchar(50),
	@Password nvarchar(50),
	@VehicleId int
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	INSERT
	INTO [DriverMaster]
	(
		[DriverName],
		[DriverFatherName],
		[DriverMobileNo],
		[DriverAddress],
		[IsActive],
		[CreatedOn],
		[UserName],
		[Password],
		[VehicleId]
	)
	VALUES
	(
		@DriverName,
		@DriverFatherName,
		@DriverMobileNo,
		@DriverAddress,
		@IsActive,
		@CreatedOn,
		@UserName,
		@Password,
		@VehicleId
	)

	SET @Err = @@Error

	SELECT @DriverId = SCOPE_IDENTITY()

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_DriverMasterLoadAll]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_DriverMasterLoadAll]
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[DriverId],
		[DriverName],
		[DriverFatherName],
		[DriverMobileNo],
		[DriverAddress],
		[IsActive],
		[CreatedOn],
		[UserName],
		[Password],
		[VehicleId]
	FROM [DriverMaster]

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_DriverMasterLoadByPrimaryKey]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_DriverMasterLoadByPrimaryKey]
(
	@DriverId int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[DriverId],
		[DriverName],
		[DriverFatherName],
		[DriverMobileNo],
		[DriverAddress],
		[IsActive],
		[CreatedOn],
		[UserName],
		[Password],
		[VehicleId]
	FROM [DriverMaster]
	WHERE
		([DriverId] = @DriverId)

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_DriverMasterLoadByUserName]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_DriverMasterLoadByUserName]
(
	@UserName nvarchar(50)
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[DriverId],
		[DriverName],
		[DriverFatherName],
		[DriverMobileNo],
		[DriverAddress],
		[IsActive],
		[CreatedOn],
		[UserName],
		[Password],
		[VehicleId]
	FROM [DriverMaster]
	WHERE
		([UserName] = @UserName) and IsActive = 'True'

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_DriverMasterUpdate]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_DriverMasterUpdate]
(
	@DriverId int,
	@DriverName nvarchar(50),
	@DriverFatherName nvarchar(50),
	@DriverMobileNo nvarchar(50),
	@DriverAddress nvarchar(MAX),
	@IsActive bit,
	@CreatedOn datetime,
	@UserName nvarchar(50),
	@Password nvarchar(50),
	@VehicleId int
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	UPDATE [DriverMaster]
	SET
		[DriverName] = @DriverName,
		[DriverFatherName] = @DriverFatherName,
		[DriverMobileNo] = @DriverMobileNo,
		[DriverAddress] = @DriverAddress,
		[IsActive] = @IsActive,
		[CreatedOn] = @CreatedOn,
		[UserName] = @UserName,
		[Password] = @Password,
		[VehicleId] = @VehicleId
	WHERE
		[DriverId] = @DriverId


	SET @Err = @@Error


	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_DropTransactionDelete]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_DropTransactionDelete]
(
	@DropId int
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	DELETE
	FROM [DropTransaction]
	WHERE
		[DropId] = @DropId
	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_DropTransactionInsert]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_DropTransactionInsert]
(
	@DropId int = NULL output,
	@StudentId int,
	@VehicleId int,
	@Latitude nvarchar(MAX),
	@Longitude nvarchar(MAX),
	@DropDateTime datetime,
	@IsPresent bit,
	@CreatedOn datetime
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	INSERT
	INTO [DropTransaction]
	(
		[StudentId],
		[VehicleId],
		[Latitude],
		[Longitude],
		[DropDateTime],
		[IsPresent],
		[CreatedOn]
	)
	VALUES
	(
		@StudentId,
		@VehicleId,
		@Latitude,
		@Longitude,
		@DropDateTime,
		@IsPresent,
		@CreatedOn
	)

	SET @Err = @@Error

	SELECT @DropId = SCOPE_IDENTITY()

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_DropTransactionLoadAll]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_DropTransactionLoadAll]
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[DropId],
		[StudentId],
		[VehicleId],
		[Latitude],
		[Longitude],
		[DropDateTime],
		[IsPresent],
		[CreatedOn]
	FROM [DropTransaction]

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_DropTransactionLoadByPrimaryKey]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_DropTransactionLoadByPrimaryKey]
(
	@DropId int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[DropId],
		[StudentId],
		[VehicleId],
		[Latitude],
		[Longitude],
		[DropDateTime],
		[IsPresent],
		[CreatedOn]
	FROM [DropTransaction]
	WHERE
		([DropId] = @DropId)

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_DropTransactionUpdate]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_DropTransactionUpdate]
(
	@DropId int,
	@StudentId int,
	@VehicleId int,
	@Latitude nvarchar(MAX),
	@Longitude nvarchar(MAX),
	@DropDateTime datetime,
	@IsPresent bit,
	@CreatedOn datetime
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	UPDATE [DropTransaction]
	SET
		[StudentId] = @StudentId,
		[VehicleId] = @VehicleId,
		[Latitude] = @Latitude,
		[Longitude] = @Longitude,
		[DropDateTime] = @DropDateTime,
		[IsPresent] = @IsPresent,
		[CreatedOn] = @CreatedOn
	WHERE
		[DropId] = @DropId


	SET @Err = @@Error


	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_LoginDetailDelete]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_LoginDetailDelete]
(
	@LoginId int
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	DELETE
	FROM [LoginDetail]
	WHERE
		[LoginId] = @LoginId
	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_LoginDetailInsert]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_LoginDetailInsert]
(
	@LoginId int = NULL output,
	@UserDisplayName nvarchar(50),
	@UserName nvarchar(50),
	@Password nvarchar(50)
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	INSERT
	INTO [LoginDetail]
	(
		[UserDisplayName],
		[UserName],
		[Password]
	)
	VALUES
	(
		@UserDisplayName,
		@UserName,
		@Password
	)

	SET @Err = @@Error

	SELECT @LoginId = SCOPE_IDENTITY()

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_LoginDetailLoadAll]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_LoginDetailLoadAll]
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[LoginId],
		[UserDisplayName],
		[UserName],
		[Password]
	FROM [LoginDetail]

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_LoginDetailLoadByPrimaryKey]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_LoginDetailLoadByPrimaryKey]
(
	@LoginId int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[LoginId],
		[UserDisplayName],
		[UserName],
		[Password]
	FROM [LoginDetail]
	WHERE
		([LoginId] = @LoginId)

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_LoginDetailLoadByUserName]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[proc_LoginDetailLoadByUserName]
(
	@UserName nvarchar(50)
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[LoginId],
		[UserDisplayName],
		[UserName],
		[Password]
	FROM [LoginDetail]
	WHERE
		([UserName] = @UserName)

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_LoginDetailUpdate]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_LoginDetailUpdate]
(
	@LoginId int,
	@UserDisplayName nvarchar(50),
	@UserName nvarchar(50),
	@Password nvarchar(50)
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	UPDATE [LoginDetail]
	SET
		[UserDisplayName] = @UserDisplayName,
		[UserName] = @UserName,
		[Password] = @Password
	WHERE
		[LoginId] = @LoginId


	SET @Err = @@Error


	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_PickupTransactionDelete]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_PickupTransactionDelete]
(
	@PickupId int
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	DELETE
	FROM [PickupTransaction]
	WHERE
		[PickupId] = @PickupId
	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_PickupTransactionInsert]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_PickupTransactionInsert]
(
	@PickupId int = NULL output,
	@StudentId int,
	@VehicleId int,
	@Latitude nvarchar(MAX),
	@Longitude nvarchar(MAX),
	@PickupDateTime datetime,
	@IsPresent bit,
	@CreatedOn datetime
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	INSERT
	INTO [PickupTransaction]
	(
		[StudentId],
		[VehicleId],
		[Latitude],
		[Longitude],
		[PickupDateTime],
		[IsPresent],
		[CreatedOn]
	)
	VALUES
	(
		@StudentId,
		@VehicleId,
		@Latitude,
		@Longitude,
		@PickupDateTime,
		@IsPresent,
		@CreatedOn
	)

	SET @Err = @@Error

	SELECT @PickupId = SCOPE_IDENTITY()

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_PickupTransactionLoadAll]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_PickupTransactionLoadAll]
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[PickupId],
		[StudentId],
		[VehicleId],
		[Latitude],
		[Longitude],
		[PickupDateTime],
		[IsPresent],
		[CreatedOn]
	FROM [PickupTransaction]

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_PickupTransactionLoadByPrimaryKey]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_PickupTransactionLoadByPrimaryKey]
(
	@PickupId int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[PickupId],
		[StudentId],
		[VehicleId],
		[Latitude],
		[Longitude],
		[PickupDateTime],
		[IsPresent],
		[CreatedOn]
	FROM [PickupTransaction]
	WHERE
		([PickupId] = @PickupId)

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_PickupTransactionUpdate]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_PickupTransactionUpdate]
(
	@PickupId int,
	@StudentId int,
	@VehicleId int,
	@Latitude nvarchar(MAX),
	@Longitude nvarchar(MAX),
	@PickupDateTime datetime,
	@IsPresent bit,
	@CreatedOn datetime
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	UPDATE [PickupTransaction]
	SET
		[StudentId] = @StudentId,
		[VehicleId] = @VehicleId,
		[Latitude] = @Latitude,
		[Longitude] = @Longitude,
		[PickupDateTime] = @PickupDateTime,
		[IsPresent] = @IsPresent,
		[CreatedOn] = @CreatedOn
	WHERE
		[PickupId] = @PickupId


	SET @Err = @@Error


	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_SectionMasterDelete]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_SectionMasterDelete]
(
	@SectionId int
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	DELETE
	FROM [SectionMaster]
	WHERE
		[SectionId] = @SectionId
	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_SectionMasterInsert]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_SectionMasterInsert]
(
	@SectionId int = NULL output,
	@SectionName nvarchar(50)
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	INSERT
	INTO [SectionMaster]
	(
		[SectionName]
	)
	VALUES
	(
		@SectionName
	)

	SET @Err = @@Error

	SELECT @SectionId = SCOPE_IDENTITY()

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_SectionMasterLoadAll]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_SectionMasterLoadAll]
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[SectionId],
		[SectionName]
	FROM [SectionMaster]

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_SectionMasterLoadByPrimaryKey]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_SectionMasterLoadByPrimaryKey]
(
	@SectionId int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[SectionId],
		[SectionName]
	FROM [SectionMaster]
	WHERE
		([SectionId] = @SectionId)

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_SectionMasterUpdate]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_SectionMasterUpdate]
(
	@SectionId int,
	@SectionName nvarchar(50)
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	UPDATE [SectionMaster]
	SET
		[SectionName] = @SectionName
	WHERE
		[SectionId] = @SectionId


	SET @Err = @@Error


	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_SessionMasterDelete]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_SessionMasterDelete]
(
	@SessionId int
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	DELETE
	FROM [SessionMaster]
	WHERE
		[SessionId] = @SessionId
	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_SessionMasterInsert]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_SessionMasterInsert]
(
	@SessionId int = NULL output,
	@SessionName nvarchar(50),
	@IsActive bit
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	INSERT
	INTO [SessionMaster]
	(
		[SessionName],
		[IsActive]
	)
	VALUES
	(
		@SessionName,
		@IsActive
	)

	SET @Err = @@Error

	SELECT @SessionId = SCOPE_IDENTITY()

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_SessionMasterLoadAll]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_SessionMasterLoadAll]
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[SessionId],
		[SessionName],
		[IsActive]
	FROM [SessionMaster]

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_SessionMasterLoadByPrimaryKey]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_SessionMasterLoadByPrimaryKey]
(
	@SessionId int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[SessionId],
		[SessionName],
		[IsActive]
	FROM [SessionMaster]
	WHERE
		([SessionId] = @SessionId)

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_SessionMasterLoadIsActive]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[proc_SessionMasterLoadIsActive]

AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[SessionId],
		[SessionName],
		[IsActive]
	FROM [SessionMaster]
	WHERE
		([IsActive] = 'True')

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_SessionMasterUpdate]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_SessionMasterUpdate]
(
	@SessionId int,
	@SessionName nvarchar(50),
	@IsActive bit
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	UPDATE [SessionMaster]
	SET
		[SessionName] = @SessionName,
		[IsActive] = @IsActive
	WHERE
		[SessionId] = @SessionId


	SET @Err = @@Error


	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_StudentMasterDelete]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_StudentMasterDelete]
(
	@StudentId int
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	DELETE
	FROM [StudentMaster]
	WHERE
		[StudentId] = @StudentId
	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_StudentMasterInsert]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_StudentMasterInsert]
(
	@StudentId int = NULL output,
	@StudentName nvarchar(50),
	@FatherName nvarchar(50),
	@MobileNo nvarchar(50),
	@AdmissionNo nvarchar(50),
	@StudentPhoto nvarchar(MAX),
	@ClassId int,
	@SectionId int,
	@Address nvarchar(MAX),
	@CreatedOn datetime,
	@IsActive bit
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	INSERT
	INTO [StudentMaster]
	(
		[StudentName],
		[FatherName],
		[MobileNo],
		[AdmissionNo],
		[StudentPhoto],
		[ClassId],
		[SectionId],
		[Address],
		[CreatedOn],
		[IsActive]
	)
	VALUES
	(
		@StudentName,
		@FatherName,
		@MobileNo,
		@AdmissionNo,
		@StudentPhoto,
		@ClassId,
		@SectionId,
		@Address,
		@CreatedOn,
		@IsActive
	)

	SET @Err = @@Error

	SELECT @StudentId = SCOPE_IDENTITY()

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_StudentMasterLoadAll]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_StudentMasterLoadAll]
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[StudentId],
		[StudentName],
		[FatherName],
		[MobileNo],
		[AdmissionNo],
		[StudentPhoto],
		[ClassId],
		[SectionId],
		[Address],
		[CreatedOn],
		[IsActive]
	FROM [StudentMaster]

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_StudentMasterLoadByClassSection]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_StudentMasterLoadByClassSection]
(
	@ClassId int,
	@SectionId int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT *
	FROM [StudentMaster]
	WHERE
		([ClassId] = @ClassId) and ([SectionId] = @SectionId) and IsActive = 'True'

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_StudentMasterLoadByPrimaryKey]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_StudentMasterLoadByPrimaryKey]
(
	@StudentId int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[StudentId],
		[StudentName],
		[FatherName],
		[MobileNo],
		[AdmissionNo],
		[StudentPhoto],
		[ClassId],
		[SectionId],
		[Address],
		[CreatedOn],
		[IsActive]
	FROM [StudentMaster]
	WHERE
		([StudentId] = @StudentId)

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_StudentMasterUpdate]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_StudentMasterUpdate]
(
	@StudentId int,
	@StudentName nvarchar(50),
	@FatherName nvarchar(50),
	@MobileNo nvarchar(50),
	@AdmissionNo nvarchar(50),
	@StudentPhoto nvarchar(MAX),
	@ClassId int,
	@SectionId int,
	@Address nvarchar(MAX),
	@CreatedOn datetime,
	@IsActive bit
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	UPDATE [StudentMaster]
	SET
		[StudentName] = @StudentName,
		[FatherName] = @FatherName,
		[MobileNo] = @MobileNo,
		[AdmissionNo] = @AdmissionNo,
		[StudentPhoto] = @StudentPhoto,
		[ClassId] = @ClassId,
		[SectionId] = @SectionId,
		[Address] = @Address,
		[CreatedOn] = @CreatedOn,
		[IsActive] = @IsActive
	WHERE
		[StudentId] = @StudentId


	SET @Err = @@Error


	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_VehicleMasterDelete]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_VehicleMasterDelete]
(
	@VehicleId int
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	DELETE
	FROM [VehicleMaster]
	WHERE
		[VehicleId] = @VehicleId
	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_VehicleMasterInsert]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_VehicleMasterInsert]
(
	@VehicleId int = NULL output,
	@VehicleCode nvarchar(50),
	@VehicleType nvarchar(50),
	@VehicleRegNumber nvarchar(50),
	@TotalSeat nvarchar(50),
	@IsActive bit
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	INSERT
	INTO [VehicleMaster]
	(
		[VehicleCode],
		[VehicleType],
		[VehicleRegNumber],
		[TotalSeat],
		[IsActive]
	)
	VALUES
	(
		@VehicleCode,
		@VehicleType,
		@VehicleRegNumber,
		@TotalSeat,
		@IsActive
	)

	SET @Err = @@Error

	SELECT @VehicleId = SCOPE_IDENTITY()

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_VehicleMasterLoadAll]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_VehicleMasterLoadAll]
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[VehicleId],
		[VehicleCode],
		[VehicleType],
		[VehicleRegNumber],
		[TotalSeat],
		[IsActive]
	FROM [VehicleMaster]

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_VehicleMasterLoadAllV]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[proc_VehicleMasterLoadAllV]
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT *
	FROM [VVehicleMaster]

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_VehicleMasterLoadByPrimaryKey]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_VehicleMasterLoadByPrimaryKey]
(
	@VehicleId int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[VehicleId],
		[VehicleCode],
		[VehicleType],
		[VehicleRegNumber],
		[TotalSeat],
		[IsActive]
	FROM [VehicleMaster]
	WHERE
		([VehicleId] = @VehicleId)

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_VehicleMasterLoadVehicle]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_VehicleMasterLoadVehicle]
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[VehicleId],
		[VehicleCode] + '-' + [VehicleRegNumber] as VehicleCode,
		[VehicleType],
		[VehicleRegNumber],
		[TotalSeat],
		[IsActive]
	FROM [VehicleMaster]

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_VehicleMasterUpdate]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_VehicleMasterUpdate]
(
	@VehicleId int,
	@VehicleCode nvarchar(50),
	@VehicleType nvarchar(50),
	@VehicleRegNumber nvarchar(50),
	@TotalSeat nvarchar(50),
	@IsActive bit
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	UPDATE [VehicleMaster]
	SET
		[VehicleCode] = @VehicleCode,
		[VehicleType] = @VehicleType,
		[VehicleRegNumber] = @VehicleRegNumber,
		[TotalSeat] = @TotalSeat,
		[IsActive] = @IsActive
	WHERE
		[VehicleId] = @VehicleId


	SET @Err = @@Error


	RETURN @Err
END

GO
/****** Object:  Table [dbo].[BusAllotment]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BusAllotment](
	[AllotmentId] [int] IDENTITY(1,1) NOT NULL,
	[AllotmentCode] [nvarchar](50) NOT NULL,
	[StudentId] [int] NOT NULL,
	[VehicleId] [int] NOT NULL,
	[DisplayOrder] [int] NOT NULL,
	[SessionId] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_BusAllotment] PRIMARY KEY CLUSTERED 
(
	[AllotmentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ClassMaster]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClassMaster](
	[ClassId] [int] IDENTITY(1,1) NOT NULL,
	[ClassName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ClassMaster] PRIMARY KEY CLUSTERED 
(
	[ClassId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DriverMaster]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DriverMaster](
	[DriverId] [int] IDENTITY(1,1) NOT NULL,
	[DriverName] [nvarchar](50) NOT NULL,
	[DriverFatherName] [nvarchar](50) NOT NULL,
	[DriverMobileNo] [nvarchar](50) NOT NULL,
	[DriverAddress] [nvarchar](max) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[VehicleId] [int] NOT NULL,
 CONSTRAINT [PK_DriverMaster] PRIMARY KEY CLUSTERED 
(
	[DriverId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DropTransaction]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DropTransaction](
	[DropId] [int] IDENTITY(1,1) NOT NULL,
	[StudentId] [int] NOT NULL,
	[VehicleId] [int] NOT NULL,
	[Latitude] [nvarchar](max) NOT NULL,
	[Longitude] [nvarchar](max) NOT NULL,
	[DropDateTime] [datetime] NOT NULL,
	[IsPresent] [bit] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_DropTransaction] PRIMARY KEY CLUSTERED 
(
	[DropId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoginDetail]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoginDetail](
	[LoginId] [int] IDENTITY(1,1) NOT NULL,
	[UserDisplayName] [nvarchar](50) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_LoginDetail] PRIMARY KEY CLUSTERED 
(
	[LoginId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PickupTransaction]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PickupTransaction](
	[PickupId] [int] IDENTITY(1,1) NOT NULL,
	[StudentId] [int] NOT NULL,
	[VehicleId] [int] NOT NULL,
	[Latitude] [nvarchar](max) NOT NULL,
	[Longitude] [nvarchar](max) NOT NULL,
	[PickupDateTime] [datetime] NOT NULL,
	[IsPresent] [bit] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_PickupTransaction] PRIMARY KEY CLUSTERED 
(
	[PickupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SectionMaster]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SectionMaster](
	[SectionId] [int] IDENTITY(1,1) NOT NULL,
	[SectionName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_SectionMaster] PRIMARY KEY CLUSTERED 
(
	[SectionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SessionMaster]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SessionMaster](
	[SessionId] [int] IDENTITY(1,1) NOT NULL,
	[SessionName] [nvarchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_SessionMaster] PRIMARY KEY CLUSTERED 
(
	[SessionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[StudentMaster]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StudentMaster](
	[StudentId] [int] IDENTITY(1,1) NOT NULL,
	[StudentName] [nvarchar](50) NOT NULL,
	[FatherName] [nvarchar](50) NOT NULL,
	[MobileNo] [nvarchar](50) NOT NULL,
	[AdmissionNo] [nvarchar](50) NOT NULL,
	[StudentPhoto] [nvarchar](max) NOT NULL,
	[ClassId] [int] NOT NULL,
	[SectionId] [int] NOT NULL,
	[Address] [nvarchar](max) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_StudentMaster] PRIMARY KEY CLUSTERED 
(
	[StudentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[VehicleMaster]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VehicleMaster](
	[VehicleId] [int] IDENTITY(1,1) NOT NULL,
	[VehicleCode] [nvarchar](50) NOT NULL,
	[VehicleType] [nvarchar](50) NOT NULL,
	[VehicleRegNumber] [nvarchar](50) NOT NULL,
	[TotalSeat] [nvarchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_VehicleMaster] PRIMARY KEY CLUSTERED 
(
	[VehicleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[VBusAllotment]    Script Date: 29-May-17 7:50:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VBusAllotment]
AS
SELECT        dbo.BusAllotment.AllotmentId, dbo.BusAllotment.AllotmentCode, dbo.BusAllotment.StudentId, dbo.BusAllotment.VehicleId, dbo.BusAllotment.DisplayOrder, dbo.BusAllotment.SessionId, 
                         dbo.BusAllotment.CreatedOn, dbo.StudentMaster.StudentName, dbo.StudentMaster.FatherName, dbo.StudentMaster.MobileNo, dbo.StudentMaster.AdmissionNo, dbo.StudentMaster.ClassId, 
                         dbo.StudentMaster.SectionId, dbo.StudentMaster.Address, dbo.ClassMaster.ClassName, dbo.SectionMaster.SectionName, dbo.StudentMaster.IsActive, dbo.StudentMaster.StudentPhoto
FROM            dbo.BusAllotment INNER JOIN
                         dbo.StudentMaster ON dbo.BusAllotment.StudentId = dbo.StudentMaster.StudentId INNER JOIN
                         dbo.ClassMaster ON dbo.StudentMaster.ClassId = dbo.ClassMaster.ClassId INNER JOIN
                         dbo.SectionMaster ON dbo.StudentMaster.SectionId = dbo.SectionMaster.SectionId

GO
SET IDENTITY_INSERT [dbo].[BusAllotment] ON 

GO
INSERT [dbo].[BusAllotment] ([AllotmentId], [AllotmentCode], [StudentId], [VehicleId], [DisplayOrder], [SessionId], [CreatedOn]) VALUES (2, N'MIS-4-2', 4, 1, 2, 1, CAST(0x0000A78100B64743 AS DateTime))
GO
INSERT [dbo].[BusAllotment] ([AllotmentId], [AllotmentCode], [StudentId], [VehicleId], [DisplayOrder], [SessionId], [CreatedOn]) VALUES (5, N'MIS-3-5', 3, 2, 2, 1, CAST(0x0000A78100B64743 AS DateTime))
GO
INSERT [dbo].[BusAllotment] ([AllotmentId], [AllotmentCode], [StudentId], [VehicleId], [DisplayOrder], [SessionId], [CreatedOn]) VALUES (6, N'MIS-1-6', 1, 1, 1, 1, CAST(0x0000A78100B6473F AS DateTime))
GO
INSERT [dbo].[BusAllotment] ([AllotmentId], [AllotmentCode], [StudentId], [VehicleId], [DisplayOrder], [SessionId], [CreatedOn]) VALUES (7, N'MIS-2-7', 2, 2, 1, 1, CAST(0x0000A78100B64741 AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[BusAllotment] OFF
GO
SET IDENTITY_INSERT [dbo].[ClassMaster] ON 

GO
INSERT [dbo].[ClassMaster] ([ClassId], [ClassName]) VALUES (1, N'1st')
GO
INSERT [dbo].[ClassMaster] ([ClassId], [ClassName]) VALUES (2, N'2nd')
GO
SET IDENTITY_INSERT [dbo].[ClassMaster] OFF
GO
SET IDENTITY_INSERT [dbo].[DriverMaster] ON 

GO
INSERT [dbo].[DriverMaster] ([DriverId], [DriverName], [DriverFatherName], [DriverMobileNo], [DriverAddress], [IsActive], [CreatedOn], [UserName], [Password], [VehicleId]) VALUES (1, N'Driver-1', N'', N'99', N'', 1, CAST(0x0000A7810104DABB AS DateTime), N'D1', N'123', 1)
GO
INSERT [dbo].[DriverMaster] ([DriverId], [DriverName], [DriverFatherName], [DriverMobileNo], [DriverAddress], [IsActive], [CreatedOn], [UserName], [Password], [VehicleId]) VALUES (2, N'Driver-2', N'', N'9754989829', N'', 1, CAST(0x0000A78100C05471 AS DateTime), N'D2', N'123', 2)
GO
SET IDENTITY_INSERT [dbo].[DriverMaster] OFF
GO
SET IDENTITY_INSERT [dbo].[LoginDetail] ON 

GO
INSERT [dbo].[LoginDetail] ([LoginId], [UserDisplayName], [UserName], [Password]) VALUES (2, N'Magadham', N'admin', N'admin@123')
GO
INSERT [dbo].[LoginDetail] ([LoginId], [UserDisplayName], [UserName], [Password]) VALUES (3, N'Pradeep Lodhi', N'pradeep', N'123')
GO
SET IDENTITY_INSERT [dbo].[LoginDetail] OFF
GO
SET IDENTITY_INSERT [dbo].[PickupTransaction] ON 

GO
INSERT [dbo].[PickupTransaction] ([PickupId], [StudentId], [VehicleId], [Latitude], [Longitude], [PickupDateTime], [IsPresent], [CreatedOn]) VALUES (7, 1, 1, N'', N'', CAST(0x0000A782007FE7B5 AS DateTime), 0, CAST(0x0000A78200000000 AS DateTime))
GO
INSERT [dbo].[PickupTransaction] ([PickupId], [StudentId], [VehicleId], [Latitude], [Longitude], [PickupDateTime], [IsPresent], [CreatedOn]) VALUES (8, 4, 1, N'', N'', CAST(0x0000A7820080C4CE AS DateTime), 1, CAST(0x0000A78200000000 AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[PickupTransaction] OFF
GO
SET IDENTITY_INSERT [dbo].[SectionMaster] ON 

GO
INSERT [dbo].[SectionMaster] ([SectionId], [SectionName]) VALUES (1, N'A')
GO
INSERT [dbo].[SectionMaster] ([SectionId], [SectionName]) VALUES (2, N'B')
GO
INSERT [dbo].[SectionMaster] ([SectionId], [SectionName]) VALUES (3, N'C')
GO
SET IDENTITY_INSERT [dbo].[SectionMaster] OFF
GO
SET IDENTITY_INSERT [dbo].[SessionMaster] ON 

GO
INSERT [dbo].[SessionMaster] ([SessionId], [SessionName], [IsActive]) VALUES (1, N'2017-2018', 1)
GO
SET IDENTITY_INSERT [dbo].[SessionMaster] OFF
GO
SET IDENTITY_INSERT [dbo].[StudentMaster] ON 

GO
INSERT [dbo].[StudentMaster] ([StudentId], [StudentName], [FatherName], [MobileNo], [AdmissionNo], [StudentPhoto], [ClassId], [SectionId], [Address], [CreatedOn], [IsActive]) VALUES (1, N'Anik Dubay', N'Ramesh Dubay', N'9993184672', N'1001', N'StudentPhoto/RN7O50ABXDPvj27May2017072719909933.jpg', 1, 1, N'Vidisha', CAST(0x0000A78001684788 AS DateTime), 1)
GO
INSERT [dbo].[StudentMaster] ([StudentId], [StudentName], [FatherName], [MobileNo], [AdmissionNo], [StudentPhoto], [ClassId], [SectionId], [Address], [CreatedOn], [IsActive]) VALUES (2, N'Anil Yadav', N'', N'9754989829', N'1002', N'', 1, 1, N'Pital Mil Vidisha', CAST(0x0000A78001679939 AS DateTime), 1)
GO
INSERT [dbo].[StudentMaster] ([StudentId], [StudentName], [FatherName], [MobileNo], [AdmissionNo], [StudentPhoto], [ClassId], [SectionId], [Address], [CreatedOn], [IsActive]) VALUES (3, N'Poonam', N'', N'9993184672', N'1003', N'', 1, 1, N'ArihantVihar Vidisha', CAST(0x0000A7800167D747 AS DateTime), 1)
GO
INSERT [dbo].[StudentMaster] ([StudentId], [StudentName], [FatherName], [MobileNo], [AdmissionNo], [StudentPhoto], [ClassId], [SectionId], [Address], [CreatedOn], [IsActive]) VALUES (4, N'Prashant Sharma', N'', N'9993184672', N'1004', N'', 1, 1, N'Block Colony', CAST(0x0000A7800168956A AS DateTime), 1)
GO
SET IDENTITY_INSERT [dbo].[StudentMaster] OFF
GO
SET IDENTITY_INSERT [dbo].[VehicleMaster] ON 

GO
INSERT [dbo].[VehicleMaster] ([VehicleId], [VehicleCode], [VehicleType], [VehicleRegNumber], [TotalSeat], [IsActive]) VALUES (1, N'01', N'Van', N'MP 40 H 0267', N'18', 1)
GO
INSERT [dbo].[VehicleMaster] ([VehicleId], [VehicleCode], [VehicleType], [VehicleRegNumber], [TotalSeat], [IsActive]) VALUES (2, N'02', N'Van', N'CG04E6077', N'15', 1)
GO
SET IDENTITY_INSERT [dbo].[VehicleMaster] OFF
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UK_UserName]    Script Date: 29-May-17 7:50:43 AM ******/
ALTER TABLE [dbo].[DriverMaster] ADD  CONSTRAINT [UK_UserName] UNIQUE NONCLUSTERED 
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Auto, Bus, Van' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'VehicleMaster', @level2type=N'COLUMN',@level2name=N'VehicleType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "BusAllotment"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 201
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "StudentMaster"
            Begin Extent = 
               Top = 0
               Left = 302
               Bottom = 210
               Right = 472
            End
            DisplayFlags = 280
            TopColumn = 3
         End
         Begin Table = "ClassMaster"
            Begin Extent = 
               Top = 6
               Left = 510
               Bottom = 109
               Right = 680
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SectionMaster"
            Begin Extent = 
               Top = 45
               Left = 706
               Bottom = 141
               Right = 876
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 18
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VBusAllotment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VBusAllotment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VBusAllotment'
GO
