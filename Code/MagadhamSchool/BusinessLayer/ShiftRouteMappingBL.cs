﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using System.Data;
using System.Data.Objects;

namespace BusinessLayer
{
    public class ShiftRouteMappingBL
    {
        public int InsertShiftRouteMapping(int ShiftId, int RouteId)
        {
            ObjectParameter MappingId = new ObjectParameter("MappingId", typeof(Int32));

            DBEntities Obj = new DBEntities();

            Obj.InsertShiftRouteMapping(MappingId, ShiftId, RouteId);

            return int.Parse(MappingId.Value.ToString());
        }

        public int UpdateShiftRouteMapping(int MappingId, int ShiftId, int RouteId)
        {
            DBEntities Obj = new DBEntities();

            Obj.UpdateShiftRouteMapping(MappingId, ShiftId, RouteId);

            return MappingId;
        }

        public int DeleteShiftRouteMapping(int MappingId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.DeleteShiftRouteMapping(MappingId);
        }

        public List<ShiftRouteMapping> LoadShiftRouteMappingByPrimaryKey(int MappingId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadShiftRouteMappingByPrimaryKey(MappingId).ToList();
        }

        public List<ShiftRouteMapping> LoadAllShiftRouteMapping()
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadAllShiftRouteMapping().ToList();
        }
        public List<VShiftRouteMapping> LoadAllShiftRouteMappingV()
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadAllShiftRouteMappingV().ToList();
        }
        public List<VShiftRouteMapping> LoadShiftRouteMappingByShiftId(int ShiftId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadShiftRouteMappingByShiftId(ShiftId).ToList();
        }
    }
}
