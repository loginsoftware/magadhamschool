﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using System.Data;
using System.Data.Objects;

namespace BusinessLayer
{
    public class ShiftMasterBL
    {
        public int InsertShiftMaster(string ShiftName)
        {
            ObjectParameter ShiftId = new ObjectParameter("ShiftId", typeof(Int32));

            DBEntities Obj = new DBEntities();

            Obj.InsertShiftMaster(ShiftId, ShiftName);

            return int.Parse(ShiftId.Value.ToString());
        }

        public int UpdateShiftMaster(int ShiftId, string ShiftName)
        {
            DBEntities Obj = new DBEntities();

            Obj.UpdateShiftMaster(ShiftId, ShiftName);

            return ShiftId;
        }

        public int DeleteShiftMaster(int ShiftId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.DeleteShiftMaster(ShiftId);
        }

        public List<ShiftMaster> LoadShiftMasterByPrimaryKey(int ShiftId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadShiftMasterByPrimaryKey(ShiftId).ToList();
        }

        public List<ShiftMaster> LoadAllShiftMaster()
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadAllShiftMaster().ToList();
        }
    }
}
