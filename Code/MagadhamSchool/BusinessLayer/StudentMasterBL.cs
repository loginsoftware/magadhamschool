﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using System.Data;
using System.Data.Objects;

namespace BusinessLayer
{
    public class StudentMasterBL
    {
        public int InsertStudentMaster(string StudentName, string FatherName, string MobileNo, string AdmissionNo, string StudentPhoto, int ClassId, int SectionId, string Address, DateTime CreatedOn, bool IsActive)
        {
            ObjectParameter StudentId = new ObjectParameter("StudentId", typeof(Int32));

            DBEntities Obj = new DBEntities();

            Obj.InsertStudentMaster(StudentId, StudentName, FatherName, MobileNo, AdmissionNo, StudentPhoto, ClassId, SectionId, Address, CreatedOn, IsActive);

            return int.Parse(StudentId.Value.ToString());
        }

        public int UpdateStudentMaster(int StudentId, string StudentName, string FatherName, string MobileNo, string AdmissionNo, string StudentPhoto, int ClassId, int SectionId, string Address, DateTime CreatedOn, bool IsActive)
        {
            DBEntities Obj = new DBEntities();

            Obj.UpdateStudentMaster(StudentId, StudentName, FatherName, MobileNo, AdmissionNo, StudentPhoto, ClassId, SectionId, Address, CreatedOn, IsActive);

            return StudentId;
        }

        public int DeleteStudentMaster(int StudentId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.DeleteStudentMaster(StudentId);
        }

        public int DeleteStudentMasterAndBusAllotment(int StudentId, int AllotmentId)
        {
            DBEntities Obj = new DBEntities();
            Obj.DeleteBusAllotment(AllotmentId);
            return Obj.DeleteStudentMaster(StudentId);
        }

        public List<StudentMaster> LoadStudentMasterByPrimaryKey(int StudentId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadStudentMasterByPrimaryKey(StudentId).ToList();
        }

        public List<StudentMaster> LoadAllStudentMaster()
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadAllStudentMaster().ToList();
        }

        public List<StudentMaster> LoadStudentMasterByClassSection(int ClassId, int SectionId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadStudentMasterByClassSection(ClassId, SectionId).ToList();
        }

        public List<VBusAllotment> LoadStudentMasterByVehicleIdRouteId(int VehicleId, int RouteId, int SessionId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadStudentMasterByVehicleIdRouteId(VehicleId, RouteId, SessionId).ToList();
        }
    }
}
