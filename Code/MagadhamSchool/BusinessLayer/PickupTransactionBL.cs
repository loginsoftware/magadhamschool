﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using System.Data;
using System.Data.Objects;

namespace BusinessLayer
{
    public class PickupTransactionBL
    {
        public int InsertPickupTransaction(int StudentId, int VehicleId, string Latitude, string Longitude, DateTime PickupDateTime, bool IsPresent, DateTime CreatedOn, int RouteId)
        {
            ObjectParameter PickupId = new ObjectParameter("PickupId", typeof(Int32));

            DBEntities Obj = new DBEntities();

            Obj.InsertPickupTransaction(PickupId, StudentId, VehicleId, Latitude, Longitude, PickupDateTime, IsPresent, CreatedOn, RouteId);

            return int.Parse(PickupId.Value.ToString());
        }

        public int UpdatePickupTransaction(int PickupId, int StudentId, int VehicleId, string Latitude, string Longitude, DateTime PickupDateTime, bool IsPresent, DateTime CreatedOn, int RouteId)
        {
            DBEntities Obj = new DBEntities();

            Obj.UpdatePickupTransaction(PickupId, StudentId, VehicleId, Latitude, Longitude, PickupDateTime, IsPresent, CreatedOn, RouteId);

            return PickupId;
        }

        public int DeletePickupTransaction(int PickupId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.DeletePickupTransaction(PickupId);
        }

        public List<PickupTransaction> LoadPickupTransactionByPrimaryKey(int PickupId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadPickupTransactionByPrimaryKey(PickupId).ToList();
        }

        public List<PickupTransaction> LoadAllPickupTransaction()
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadAllPickupTransaction().ToList();
        }

        public List<TransactionPickupDropDashboard_Result> LoadPickupTransactionPickupDropDashboard(DateTime TodayDate, int SessionId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadPickupTransactionPickupDropDashboard(TodayDate, SessionId).ToList();
        }

        public List<VPickupTransaction> LoadPickupTransactionByCreatedOn(int VehicleId, int RouteId, DateTime CreatedOn)
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadPickupTransactionByCreatedOn(VehicleId, RouteId, CreatedOn).ToList();
        }
        public List<PickupTransactionPickupDateTime_Result> LoadPickupTransactionPickupDateTime(DateTime TodayDate, int RouteId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadPickupTransactionPickupDateTime(TodayDate, RouteId).ToList();
        }
    }
}
