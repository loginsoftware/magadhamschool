﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using System.Data;
using System.Data.Objects;

namespace BusinessLayer
{
    public class BusAllotmentBL
    {
        public int InsertBusAllotment(string AllotmentCode, int StudentId, Nullable<int> VehicleId,Nullable<int> RouteId, int DisplayOrder, int SessionId, DateTime CreatedOn)
        {
            ObjectParameter AllotmentId = new ObjectParameter("AllotmentId", typeof(Int32));

            DBEntities Obj = new DBEntities();

            Obj.InsertBusAllotment(AllotmentId, AllotmentCode, StudentId, VehicleId, RouteId, DisplayOrder, SessionId, CreatedOn);

            return int.Parse(AllotmentId.Value.ToString());
        }

        public int UpdateBusAllotment(int AllotmentId, string AllotmentCode, int StudentId, Nullable<int> VehicleId, Nullable<int> RouteId, int DisplayOrder, int SessionId, DateTime CreatedOn)
        {
            DBEntities Obj = new DBEntities();

            Obj.UpdateBusAllotment(AllotmentId, AllotmentCode, StudentId, VehicleId, RouteId, DisplayOrder, SessionId, CreatedOn);

            return AllotmentId;
        }

        public int DeleteBusAllotment(int AllotmentId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.DeleteBusAllotment(AllotmentId);
        }

        public List<BusAllotment> LoadBusAllotmentByPrimaryKey(int AllotmentId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadBusAllotmentByPrimaryKey(AllotmentId).ToList();
        }

        public List<BusAllotment> LoadAllBusAllotment()
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadAllBusAllotment().ToList();
        }

        public List<BusAllotment> LoadBusAllotmentByStudentIdSessionId(int StudentId, int SessionId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadBusAllotmentByStudentIdSessionId(StudentId, SessionId).ToList();
        }

        public int UpdateBusAllotmentAllotmentCode(int AllotmentId, string AllotmentCode)
        {
            DBEntities Obj = new DBEntities();

            Obj.UpdateBusAllotmentAllotmentCode(AllotmentId, AllotmentCode);

            return AllotmentId;
        }

        public List<VBusAllotment> LoadBusAllotmentByVehicleId(int VehicleId, int SessionId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadBusAllotmentByVehicleId(VehicleId, SessionId).ToList();
        }
        public int UpdateBusAllotmentDisplayOrder(int AllotmentId, int DisplayOrder)
        {
            DBEntities Obj = new DBEntities();

            Obj.UpdateBusAllotmentDisplayOrder(AllotmentId, DisplayOrder);

            return AllotmentId;
        }

        public List<VBusAllotment> LoadBusAllotmentForPickup(int VehicleId, int RouteId, int SessionId, DateTime TodayDate)
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadBusAllotmentForPickup(VehicleId, RouteId, SessionId, TodayDate).ToList();
        }

        public List<VBusAllotment> LoadBusAllotmentForDrop(int VehicleId, int RouteId, int SessionId, DateTime TodayDate)
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadBusAllotmentForDrop(VehicleId, RouteId, SessionId, TodayDate).ToList();
        }

        public List<BusAllotmentPickupDropTxn_Result> LoadBusAllotmentPickupDropTxn(int VehicleId, int SessionId, DateTime TodayDate, DateTime YesterdayDate, DateTime DayBeforeYesterdayDate)
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadBusAllotmentPickupDropTxn(VehicleId, SessionId, TodayDate, YesterdayDate, DayBeforeYesterdayDate).ToList();
        }

        public List<VBusAllotment> LoadBusAllotmentByAllotmentReciept(int AllotmentId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadBusAllotmentByAllotmentReciept(AllotmentId).ToList();
        }
    }
}
