﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using System.Data;
using System.Data.Objects;

namespace BusinessLayer
{
    public class SessionMasterBL
    {
        public int InsertSessionMaster(string SessionName, bool IsActive)
        {
            ObjectParameter SessionId = new ObjectParameter("SessionId", typeof(Int32));

            DBEntities Obj = new DBEntities();

            Obj.InsertSessionMaster(SessionId, SessionName, IsActive);

            return int.Parse(SessionId.Value.ToString());
        }

        public int UpdateSessionMaster(int SessionId, string SessionName, bool IsActive)
        {
            DBEntities Obj = new DBEntities();

            Obj.UpdateSessionMaster(SessionId, SessionName, IsActive);

            return SessionId;
        }

        public int DeleteSessionMaster(int SessionId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.DeleteSessionMaster(SessionId);
        }

        public List<SessionMaster> LoadSessionMasterByPrimaryKey(int SessionId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadSessionMasterByPrimaryKey(SessionId).ToList();
        }

        public List<SessionMaster> LoadAllSessionMaster()
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadAllSessionMaster().ToList();
        }

        public List<SessionMaster> LoadSessionMasterIsActive()
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadSessionMasterIsActive().ToList();
        }
    }
}
