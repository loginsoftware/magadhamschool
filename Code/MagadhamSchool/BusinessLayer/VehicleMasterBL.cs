﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using System.Data;
using System.Data.Objects;

namespace BusinessLayer
{
    public class VehicleMasterBL
    {
        public int InsertVehicleMaster(string VehicleCode, string VehicleType, string VehicleRegNumber, string TotalSeat, bool IsActive, int DriverId)
        {
            ObjectParameter VehicleId = new ObjectParameter("VehicleId", typeof(Int32));

            DBEntities Obj = new DBEntities();

            Obj.InsertVehicleMaster(VehicleId, VehicleCode, VehicleType, VehicleRegNumber, TotalSeat, IsActive, DriverId);

            return int.Parse(VehicleId.Value.ToString());
        }

        public int UpdateVehicleMaster(int VehicleId, string VehicleCode, string VehicleType, string VehicleRegNumber, string TotalSeat, bool IsActive, int DriverId)
        {
            DBEntities Obj = new DBEntities();

            Obj.UpdateVehicleMaster(VehicleId, VehicleCode, VehicleType, VehicleRegNumber, TotalSeat, IsActive, DriverId);

            return VehicleId;
        }

        public int DeleteVehicleMaster(int VehicleId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.DeleteVehicleMaster(VehicleId);
        }

        public List<VehicleMaster> LoadVehicleMasterByPrimaryKey(int VehicleId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadVehicleMasterByPrimaryKey(VehicleId).ToList();
        }

        public List<VehicleMaster> LoadAllVehicleMaster()
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadAllVehicleMaster().ToList();
        }
        public List<VVehicleMaster> LoadAllVehicleMasterV()
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadAllVehicleMasterV().ToList();
        }
        public List<VehicleMaster> LoadVehicleMasterVehicle()
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadVehicleMasterVehicle().ToList();
        }

        public List<VehicleMaster> LoadVehicleMasterByDriverId(int DriverId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadVehicleMasterByDriverId(DriverId).ToList();
        }

        public List<VVehicleMaster> LoadVehicleMasterByDriverIdV(int DriverId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadVehicleMasterByDriverIdV(DriverId).ToList();
        }

        public List<VVehicleMaster> LoadVehicleMasterByVehicledV(int VehicleId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadVehicleMasterByVehicleIdV(VehicleId).ToList();
        }
    }
}
