﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using System.Data;
using System.Data.Objects;

namespace BusinessLayer
{
    public class DriverMasterBL
    {
        public int InsertDriverMaster(string DriverName, string DriverFatherName, string DriverMobileNo, string DriverAddress, bool IsAsctive, DateTime CreatedOn, string UserName, string Password)
        {
            ObjectParameter DriverId = new ObjectParameter("DriverId", typeof(Int32));

            DBEntities Obj = new DBEntities();

            Obj.InsertDriverMaster(DriverId, DriverName, DriverFatherName, DriverMobileNo, DriverAddress, IsAsctive, CreatedOn, UserName, Password);

            return int.Parse(DriverId.Value.ToString());
        }

        public int UpdateDriverMaster(int DriverId, string DriverName, string DriverFatherName, string DriverMobileNo, string DriverAddress, bool IsAsctive, DateTime CreatedOn, string UserName, string Password)
        {
            DBEntities Obj = new DBEntities();

            Obj.UpdateDriverMaster(DriverId, DriverName, DriverFatherName, DriverMobileNo, DriverAddress, IsAsctive, CreatedOn, UserName, Password);

            return DriverId;
        }

        public int DeleteDriverMaster(int DriverId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.DeleteDriverMaster(DriverId);
        }

        public List<DriverMaster> LoadDriverMasterByPrimaryKey(int DriverId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadDriverMasterByPrimaryKey(DriverId).ToList();
        }

        public List<DriverMaster> LoadAllDriverMaster()
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadAllDriverMaster().ToList();
        }

        public List<DriverMaster> LoadDriverMasterByUserName(string UserName)
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadDriverMasterByUserName(UserName).ToList();
        }
    }
}
