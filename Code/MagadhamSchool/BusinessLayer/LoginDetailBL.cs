﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using System.Data;
using System.Data.Objects;

namespace BusinessLayer
{
    public class LoginDetailBL
    {
        public int InsertLoginDetail(string UserDisplayName, string UserName, string Password)
        {
            ObjectParameter LoginId = new ObjectParameter("LoginId", typeof(Int32));

            DBEntities Obj = new DBEntities();

            Obj.InsertLoginDetail(LoginId, UserDisplayName, UserName, Password);

            return int.Parse(LoginId.Value.ToString());
        }

        public int UpdateLoginDetail(int LoginId, string UserDisplayName, string UserName, string Password)
        {
            DBEntities Obj = new DBEntities();

            Obj.UpdateLoginDetail(LoginId, UserDisplayName, UserName, Password);

            return LoginId;
        }

        public int DeleteLoginDetail(int LoginId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.DeleteLoginDetail(LoginId);
        }

        public List<LoginDetail> LoadLoginDetailByPrimaryKey(int LoginId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadLoginDetailByPrimaryKey(LoginId).ToList();
        }

        public List<LoginDetail> LoadAllLoginDetail()
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadAllLoginDetail().ToList();
        }

        public List<LoginDetail> LoadLoginDetailByUserName(string UserName)
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadLoginDetailByUserName(UserName).ToList();
        }
    }
}
