﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using System.Data;
using System.Data.Objects;

namespace BusinessLayer
{
    public class SchoolReachTxnBL
    {
        public int InsertSchoolReachTxn(int VehicleId, int RouteId, DateTime ReachDatetime, DateTime CreatedOn, string TxnType)
        {
            ObjectParameter ReachId = new ObjectParameter("ReachId", typeof(Int32));

            DBEntities Obj = new DBEntities();

            Obj.InsertSchoolReachTxn(ReachId, VehicleId, RouteId, ReachDatetime, CreatedOn, TxnType);

            return int.Parse(ReachId.Value.ToString());
        }

        public int UpdateSchoolReachTxn(int ReachId, int VehicleId, int RouteId, DateTime ReachDatetime, DateTime CreatedOn, string TxnType)
        {
            DBEntities Obj = new DBEntities();

            Obj.UpdateSchoolReachTxn(ReachId, VehicleId, RouteId, ReachDatetime, CreatedOn, TxnType);

            return ReachId;
        }

        public int DeleteSchoolReachTxn(int ReachId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.DeleteSchoolReachTxn(ReachId);
        }

        public List<SchoolReachTxn> LoadSchoolReachTxnByPrimaryKey(int ReachId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadSchoolReachTxnByPrimaryKey(ReachId).ToList();
        }

        public List<SchoolReachTxn> LoadAllSchoolReachTxn()
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadAllSchoolReachTxn().ToList();
        }

        public List<SchoolReachTxn> LoadSchoolReachTxnByCreatedOn(int VehicleId, int RouteId, DateTime CreatedOn, string TxnType)
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadSchoolReachTxnByCreatedOn(VehicleId, RouteId, CreatedOn, TxnType).ToList();
        }
    }
}
