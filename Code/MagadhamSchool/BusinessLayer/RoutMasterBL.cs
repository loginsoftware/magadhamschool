﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using System.Data;
using System.Data.Objects;

namespace BusinessLayer
{
    public class RoutMasterBL
    {
        public int InsertRoutMaster(string RouteName, string RouteDescription, int VehicleId)
        {
            ObjectParameter RouteId = new ObjectParameter("RouteId", typeof(Int32));

            DBEntities Obj = new DBEntities();

            Obj.InsertRoutMaster(RouteId, RouteName, RouteDescription, VehicleId);

            return int.Parse(RouteId.Value.ToString());
        }

        public int UpdateRoutMaster(int RouteId, string RouteName, string RouteDescription, int VehicleId)
        {
            DBEntities Obj = new DBEntities();

            Obj.UpdateRoutMaster(RouteId, RouteName, RouteDescription, VehicleId);

            return RouteId;
        }

        public int DeleteRoutMaster(int RouteId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.DeleteRoutMaster(RouteId);
        }

        public List<RoutMaster> LoadRoutMasterByPrimaryKey(int RouteId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadRoutMasterByPrimaryKey(RouteId).ToList();
        }

        public List<RoutMaster> LoadAllRoutMaster()
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadAllRoutMaster().ToList();
        }
        public List<VRouteMaster> LoadAllRoutMasterV()
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadAllRoutMasterV().ToList();
        }

        public List<RoutMaster> LoadRoutMasterByVehicleId(int VehicleId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadRoutMasterByVehicleId(VehicleId).ToList();
        }
       
    }
}
