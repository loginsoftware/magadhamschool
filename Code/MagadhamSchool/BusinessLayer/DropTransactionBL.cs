﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using System.Data;
using System.Data.Objects;

namespace BusinessLayer
{
    public class DropTransactionBL
    {
        public int InsertDropTransaction(int StudentId, int VehicleId, string Latitude, string Longitude, DateTime DropDateTime, bool IsPresent, DateTime CreatedOn, int RouteId)
        {
            ObjectParameter DropId = new ObjectParameter("DropId", typeof(Int32));

            DBEntities Obj = new DBEntities();

            Obj.InsertDropTransaction(DropId, StudentId, VehicleId, Latitude, Longitude, DropDateTime, IsPresent, CreatedOn, RouteId);

            return int.Parse(DropId.Value.ToString());
        }

        public int UpdateDropTransaction(int DropId, int StudentId, int VehicleId, string Latitude, string Longitude, DateTime DropDateTime, bool IsPresent, DateTime CreatedOn, int RouteId)
        {
            DBEntities Obj = new DBEntities();

            Obj.UpdateDropTransaction(DropId, StudentId, VehicleId, Latitude, Longitude, DropDateTime, IsPresent, CreatedOn, RouteId);

            return DropId;
        }

        public int DeleteDropTransaction(int DropId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.DeleteDropTransaction(DropId);
        }

        public List<DropTransaction> LoadDropTransactionByPrimaryKey(int DropId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadDropTransactionByPrimaryKey(DropId).ToList();
        }

        public List<DropTransaction> LoadAllDropTransaction()
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadAllDropTransaction().ToList();
        }

        public List<DropTransactionDropDateTime_Result> LoadDropTransactionDropDateTime(DateTime TodayDate, int RouteId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadDropTransactionDropDateTime(TodayDate, RouteId).ToList();
        }
    }
}
