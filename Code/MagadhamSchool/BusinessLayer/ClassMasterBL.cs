﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using System.Data;
using System.Data.Objects;

namespace BusinessLayer
{
    public class ClassMasterBL
    {
        public int InsertClassMaster(string ClassName)
        {
            ObjectParameter ClassId = new ObjectParameter("ClassId", typeof(Int32));

            DBEntities Obj = new DBEntities();

            Obj.InsertClassMaster(ClassId, ClassName);

            return int.Parse(ClassId.Value.ToString());
        }

        public int UpdateClassMaster(int ClassId, string ClassName)
        {
            DBEntities Obj = new DBEntities();

            Obj.UpdateClassMaster(ClassId, ClassName);

            return ClassId;
        }

        public int DeleteClassMaster(int ClassId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.DeleteClassMaster(ClassId);
        }

        public List<ClassMaster> LoadClassMasterByPrimaryKey(int ClassId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadClassMasterByPrimaryKey(ClassId).ToList();
        }

        public List<ClassMaster> LoadAllClassMaster()
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadAllClassMaster().ToList();
        }
    }
}
