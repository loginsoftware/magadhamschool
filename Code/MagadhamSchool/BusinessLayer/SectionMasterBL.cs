﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using System.Data;
using System.Data.Objects;

namespace BusinessLayer
{
    public class SectionMasterBL
    {
        public int InsertSectionMaster(string SectionName)
        {
            ObjectParameter SectionId = new ObjectParameter("SectionId", typeof(Int32));

            DBEntities Obj = new DBEntities();

            Obj.InsertSectionMaster(SectionId, SectionName);

            return int.Parse(SectionId.Value.ToString());
        }

        public int UpdateSectionMaster(int SectionId, string SectionName)
        {
            DBEntities Obj = new DBEntities();

            Obj.UpdateSectionMaster(SectionId, SectionName);

            return SectionId;
        }

        public int DeleteSectionMaster(int SectionId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.DeleteSectionMaster(SectionId);
        }

        public List<SectionMaster> LoadSectionMasterByPrimaryKey(int SectionId)
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadSectionMasterByPrimaryKey(SectionId).ToList();
        }

        public List<SectionMaster> LoadAllSectionMaster()
        {
            DBEntities Obj = new DBEntities();

            return Obj.LoadAllSectionMaster().ToList();
        }
    }
}
