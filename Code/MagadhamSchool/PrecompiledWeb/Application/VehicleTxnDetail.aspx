﻿<%@ page title="" language="C#" masterpagefile="~/Site.master" autoeventwireup="true" inherits="VehicleTxnDetail, App_Web_1qaf3qzc" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
    <div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-edit"></i>Pickup Drop Detail</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <div role="form">
                    <div class="row form-group">
                    <div class="col-md-3">
                        <label>Date:</label>
    <asp:TextBox ID="TxtDate" runat="server" CssClass="form-control"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender1" TargetControlID="TxtDate" PopupButtonID="TxtDate" runat="server" Format="dd-MMM-yyyy">
                        </asp:CalendarExtender>
                        </div>
                         <div class="col-md-3">
                             <br />
                             <asp:RadioButtonList ID="rbTxnType" runat="server" RepeatDirection="Horizontal">
                                 <asp:ListItem Value="Pickup" Selected="True">Pickup</asp:ListItem>
                                 <asp:ListItem Value="Drop">Drop</asp:ListItem>
                             </asp:RadioButtonList>
                        </div>
                          <div class="col-md-3">
                        <label>Vehicle</label><br />
               <asp:DropDownList ID="DdnVehicle" runat="server" AppendDataBoundItems="true" Width="100%" Height="39px" AutoPostBack="True" OnSelectedIndexChanged="DdnVehicle_SelectedIndexChanged">
                 <asp:ListItem>--Select Vehicle--</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-3">
                        <label>Route</label><br />
                 <asp:DropDownList ID="DdnRoute" runat="server" Width="100%" Height="39px" AppendDataBoundItems="true">
                     <asp:ListItem>--Select Route--</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    </div>
                     <div class="row">
                   <div class="col-md-4">
                    <asp:LinkButton ID="BtnShow" runat="server" CssClass="btn btn-default" OnClick="BtnShow_Click">Show</asp:LinkButton>
                    <a class="btn btn-default" onclick="window.location.href = 'VehicleTxnDetail.aspx'">Reset</a>

    <asp:HiddenField ID="hfPKey" runat="server" />
     </div>
     </div>

                </div>

            </div>
        </div>
    </div>
    <!--/span-->

</div>



<div class="row">
        <div class="box col-md-12">
            <div class="box-inner">
                <div class="box-header well" data-original-title="">
                    <h2><i class="glyphicon glyphicon-user"></i> Vehicle Report</h2>

                    <div class="box-icon">
                       <a href="#" onclick="printdiv('printData');" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-print"></i> Print</a> &nbsp;&nbsp;&nbsp;
                    </div>
                </div>
                <div id="printData" class="box-content">
                                      <table style="width:100%">
                                      <tr><td style="text-align:center;font-size:26px;">Magadham Internation School</td></tr>
                                      <tr><td style="text-align:center;font-size:16px;">Report- Student Pickup/Drop</td></tr>
                                      <tr><td style="text-align:center;font-size:16px;">Vehicle: 
                                          <asp:Label ID="lblVehicleDriverName" runat="server" Text=""></asp:Label></td></tr>
                                      </table>

                                  <asp:GridView ID="GVDetail" AutoGenerateColumns="False" AllowPaging="false" 
            runat="server" DataKeyNames="StudentId" EmptyDataText="No Record Found" 
            Width="100%" CssClass="table table-striped table-bordered responsive">
                            <AlternatingRowStyle BackColor="#ecebeb" />
                            <Columns>
                    <asp:TemplateField HeaderText="Sr.No" HeaderStyle-Width="10px">
            <ItemTemplate>
                <asp:Label ID="lblSRNO" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                <asp:HiddenField ID="hfStudentId" runat="server" Value='<%# Eval("StudentId") %>' />

            </ItemTemplate>

<HeaderStyle Width="10px"></HeaderStyle>
            </asp:TemplateField>
                            <asp:TemplateField HeaderText="AllotmentCode">
                            <ItemTemplate>
                 <asp:Label ID="lblAdmissionNo" runat="server" Text='<%# Eval("AllotmentCode") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Student Name">
                            <ItemTemplate>
                                <asp:Label ID="lblStudentName" runat="server" Text='<%# Eval("StudentName") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>                             
                             <asp:TemplateField HeaderText="Mobile">
                            <ItemTemplate>
                                <asp:Label ID="lblMobileNo" runat="server" Text='<%# Eval("MobileNo") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Address">
                            <ItemTemplate>
                                <asp:Label ID="lblAddress" runat="server" Text='<%# Eval("Address") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                           <asp:TemplateField HeaderText="Today Date">
                           <HeaderTemplate>
                               <asp:Label ID="lblTodayDate" runat="server" Text=""></asp:Label>
                           </HeaderTemplate>
                            <ItemTemplate>
                  <asp:Label ID="lblTodaysPickup" runat="server" Text='<%# Eval("TodaysPickup", "{0:dd-MMM-yyyy hh:mm tt}") %>'></asp:Label>
                   <asp:Label ID="lblTodaysDrop" runat="server" Text='<%# Eval("TodaysDrop","{0:dd-MMM-yyyy hh:mm tt}") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Yesterday Date">
                           <HeaderTemplate>
                               <asp:Label ID="lblYesterdayDate" runat="server" Text=""></asp:Label>
                           </HeaderTemplate>
                            <ItemTemplate>
                  <asp:Label ID="lblYesterdayPickup" runat="server" Text='<%# Eval("YesterdayPickup","{0:dd-MMM-yyyy hh:mm tt}") %>'></asp:Label>
                   <asp:Label ID="lblYesterdayDrop" runat="server" Text='<%# Eval("YesterdayDrop","{0:dd-MMM-yyyy hh:mm tt}") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DayBeforeYesterday Date">
                           <HeaderTemplate>
                               <asp:Label ID="lblDayBeforeYesterdayDate" runat="server" Text=""></asp:Label>
                           </HeaderTemplate>
                            <ItemTemplate>
<asp:Label ID="lblDayBeforeYesterdayPickup" runat="server" Text='<%# Eval("DayBeforeYesterdayPickup") %>'></asp:Label>
    <asp:Label ID="lblDayBeforeYesterdayDrop" runat="server" Text='<%# Eval("DayBeforeYesterdayDrop") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            </Columns>
                            </asp:GridView>

                    
                </div>
            </div>
        </div>
        <!--/span-->

    </div>
    <script language="javascript">
        function printdiv(printpage) {
            var headstr = "<html><head><title></title></head><body>";
            var footstr = "</body>";
            var newstr = document.all.item(printpage).innerHTML;
            var oldstr = document.body.innerHTML;
            document.body.innerHTML = headstr + newstr + footstr;
            window.print();
            document.body.innerHTML = oldstr;
            return false;
        }
</script>
</asp:Content>

