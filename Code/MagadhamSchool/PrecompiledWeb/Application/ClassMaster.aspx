﻿<%@ page title="" language="C#" masterpagefile="~/Site.master" autoeventwireup="true" inherits="ClassMaster, App_Web_1qaf3qzc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-edit"></i>Class Master</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <div role="form">
                    <div class="form-group">
                        <label>Class Name</label>
    <asp:TextBox ID="TxtClassName" runat="server" CssClass="form-control" placeholder="Enter Class Name"></asp:TextBox>
                    </div>
                  
    <asp:LinkButton ID="BtnSave" runat="server" CssClass="btn btn-default" OnClick="BtnSave_Click">Save</asp:LinkButton>
    <asp:HiddenField ID="hfPKey" runat="server" />
                </div>

            </div>
        </div>
    </div>
    <!--/span-->

</div>

<div class="row">
        <div class="box col-md-12">
            <div class="box-inner">
                <div class="box-header well" data-original-title="">
                    <h2><i class="glyphicon glyphicon-user"></i> Class Detail</h2>

                    <div class="box-icon">
                        <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                        <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                    </div>
                </div>
                <div class="box-content">
                                  <asp:GridView ID="GVDetail" AutoGenerateColumns="False" AllowPaging="false" 
            runat="server" DataKeyNames="ClassId" EmptyDataText="No Record Found" 
            Width="100%" CssClass="table table-striped table-bordered responsive">
                            <AlternatingRowStyle BackColor="#ecebeb" />
                            <Columns>
                                 <asp:TemplateField HeaderText="Sr.No" HeaderStyle-Width="10px">
                            <ItemTemplate>
                                <asp:Label ID="lblSRNO" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Class Name">
                            <ItemTemplate>
                                <asp:Label ID="lblClassName" runat="server" Text='<%# Eval("ClassName") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                           
    <asp:TemplateField HeaderText="Action">
                      <ItemTemplate>
                   <asp:LinkButton ID="LnkBtnEdit" CssClass="btn btn-success" runat="server" onclick="LnkBtnEdit_Click"
                            ToolTip="Click here to Edit">Edit</asp:LinkButton> 
                     <asp:LinkButton ID="LnkBtnDelete" CssClass="btn btn-danger"  runat="server" 
                      OnClientClick="return confirm('Are you sure to delete this record. ?') ;" 
                            ToolTip="Click here to Delete" onclick="LnkBtnDelete_Click">Delete</asp:LinkButton>       
                      </ItemTemplate>
                      </asp:TemplateField>
                            
                            </Columns>
                            </asp:GridView>


                    
                </div>
            </div>
        </div>
        <!--/span-->

    </div>
</asp:Content>

