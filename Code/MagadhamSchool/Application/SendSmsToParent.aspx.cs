﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;

public partial class SendSmsToParent : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            FillVehicle(DdnVehicle);
        }
    }

    protected void BtnSend_Click(object sender, EventArgs e)
    {
        if (DdnVehicle.SelectedIndex > 0)
        {
            SendNotification();
        }
    }

    private void SendNotification()
    {
        BusAllotmentBL obj = new BusAllotmentBL();
        List<DataLayer.VBusAllotment> Dt = obj.LoadBusAllotmentByVehicleId(int.Parse(DdnVehicle.SelectedValue), int.Parse(GetCookie(this.Page, "SessionId")));
        if (Dt.Count > 0)
        {
            for (int i = 0; i < Dt.Count; i++)
            {
                if (Dt[i].MobileNo != "")
                {
                    SendSmsToParent(Dt[i].MobileNo, Dt[i].StudentName, TxtDelayTime.Text);
                }
            }
            ShowMessage("Notification Sent successfully", this.Page);
        }
        
    }
}