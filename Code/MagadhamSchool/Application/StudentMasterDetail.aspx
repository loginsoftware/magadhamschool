﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="StudentMasterDetail.aspx.cs" Inherits="StudentMasterDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-edit"></i>Student Master</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <div role="form">
                    <div class="row form-group">
                    <div class="col-md-4">
                        <label>Vehicle</label><br />
               <asp:DropDownList ID="DdnVehicle" runat="server" AppendDataBoundItems="true" Width="100%" Height="39px" AutoPostBack="True" OnSelectedIndexChanged="DdnVehicle_SelectedIndexChanged">
                 <asp:ListItem>--Select Vehicle--</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-4">
                        <label>Route</label><br />
                 <asp:DropDownList ID="DdnRoute" runat="server" Width="100%" Height="39px" AppendDataBoundItems="true">
                     <asp:ListItem>--Select Route--</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                   
                    </div>
                    

                    <div class="row">
                   <div class="col-md-4">
                    <asp:LinkButton ID="BtnShow" runat="server" CssClass="btn btn-default" OnClick="BtnShow_Click">Show</asp:LinkButton>
    <asp:HiddenField ID="hfPKey" runat="server" />
     </div>
     </div>
    
                </div>

            </div>
        </div>
    </div>
    <!--/span-->

</div>

<div class="row">
        <div class="box col-md-12">
            <div class="box-inner">
                <div class="box-header well" data-original-title="">
                    <h2><i class="glyphicon glyphicon-user"></i> Student Detail</h2>

                    <div class="box-icon">
                        <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                        <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                    </div>
                </div>
                <div class="box-content">
                                  <asp:GridView ID="GVDetail" AutoGenerateColumns="False" AllowPaging="false" 
            runat="server" DataKeyNames="StudentId,AllotmentId" EmptyDataText="No Record Found" 
            Width="100%" CssClass="table table-striped table-bordered responsive">
                            <AlternatingRowStyle BackColor="#ecebeb" />
                            <Columns>
                                 <asp:TemplateField HeaderText="Sr.No" HeaderStyle-Width="10px">
                            <ItemTemplate>
                                <asp:Label ID="lblSRNO" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="AdmissionNo">
                            <ItemTemplate>
                                <asp:Label ID="lblAdmissionNo" runat="server" Text='<%# Eval("AdmissionNo") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Student Name">
                            <ItemTemplate>
                                <asp:Label ID="lblStudentName" runat="server" Text='<%# Eval("StudentName") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="FatherName">
                            <ItemTemplate>
                                <asp:Label ID="lblFatherName" runat="server" Text='<%# Eval("FatherName") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Mobile">
                            <ItemTemplate>
                                <asp:Label ID="lblMobileNo" runat="server" Text='<%# Eval("MobileNo") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Address">
                            <ItemTemplate>
                                <asp:Label ID="lblAddress" runat="server" Text='<%# Eval("Address") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="IsActive">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkIsActive" runat="server" Checked='<%# Eval("IsActive") %>' Enabled="false" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CreatedOn">
                            <ItemTemplate>
                                <asp:Label ID="lblCreatedOn" runat="server" Text='<%# Eval("CreatedOn", "{0:dd-MMM-yyyy}") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Photo">
                            <ItemTemplate>
              <asp:Image ID="ImgPhoto" runat="server" ImageUrl='<%# Eval("StudentPhoto") %>' Width="30px" Height="50px" />
                            </ItemTemplate>
                            </asp:TemplateField>
    <asp:TemplateField HeaderText="Action">
                      <ItemTemplate>
                   <asp:LinkButton ID="LnkBtnEdit" CssClass="btn btn-success" runat="server" onclick="LnkBtnEdit_Click"
                            ToolTip="Click here to Edit">Edit</asp:LinkButton> 
                     <asp:LinkButton ID="LnkBtnDelete" CssClass="btn btn-danger"  runat="server" 
                      OnClientClick="return confirm('Are you sure to delete this record. ?') ;" 
                            ToolTip="Click here to Delete" onclick="LnkBtnDelete_Click">Delete</asp:LinkButton>  
                          <asp:HyperLink ID="lnkPrint" CssClass="btn btn-info"  runat="server" NavigateUrl='<%# String.Format("~/Reciept_BusAllotment.aspx?Id={0}", EncryptString(Eval("AllotmentId").ToString())) %>' Visible='<%# Eval("VehicleId") == null ? false : true %>' Target="_blank"><i class="glyphicon glyphicon-print"></i></asp:HyperLink>     
                      </ItemTemplate>
                      </asp:TemplateField>
                            
                            </Columns>
                            </asp:GridView>


                    
                </div>
            </div>
        </div>
        <!--/span-->

    </div>
</asp:Content>

