﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;

public partial class Default : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            FillSession(DdnSession);
        }
    }
    protected void BtnLogin_Click(object sender, EventArgs e)
    {
        LoginDetailBL obj = new LoginDetailBL();
        List<DataLayer.LoginDetail> L = obj.LoadLoginDetailByUserName(txtUserName.Value);
        if (L.Count > 0)
        {
            if (L[0].Password == txtPassword.Value)
            {
                SetCookie(this.Page, "LoginId", L[0].LoginId.ToString());
                SetCookie(this.Page, "SessionId", DdnSession.SelectedValue);
                SetCookie(this.Page, "SessionName", DdnSession.SelectedItem.Text);
                SetCookie(this.Page, "DisplayName", L[0].UserDisplayName);
                    
                Response.Redirect("Dashboard.aspx");             

            }
            else
            {
               ShowMessage("Invalid Password", this.Page);
            }
        }
        else
        {
           ShowMessage("Invalid UserName", this.Page);
        }
    }
}