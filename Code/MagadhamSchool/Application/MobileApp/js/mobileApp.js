﻿
//var requestUrl = "http://localhost:48374/WebService.asmx";
//var siteDomain = "http://localhost:48374/";
var requestUrl = "http://pa.somee.com/WebService.asmx";
var siteDomain = "http://pa.somee.com/";


 //localStorage.setItem("GuardId", "1");
// localStorage.setItem("SocietyId", "1");

$(document).ready(function () {

    $("#slide-out-left").load("left-sidebar-menu.html");
    setTimeout(function () {
        $("#menuDriverName").html(localStorage.getItem("DriverName"));
        $("#menuVehicleRegNo").html(localStorage.getItem("VehicleRegNumber"));
    }, 100);
});

$("#btnLogin").click(function () {

    if ($("#txtUserName").val() == "") {
        alert("Enter User Name.");
        return;
    }
    if ($("#TxtPassword").val() == "") {
        alert("Enter Password");
        return;
    }
    ShowLoader();
    var url1 = requestUrl + "/LoginUser";
     var name = $('#txtUserName').val();
    var pass = $('#TxtPassword').val();
    jQuery.ajax({
        url: url1,
        type: "POST",
        dataType: "json",
        data: "{'UserName':'" + name + "','Password':'" + pass + "'}",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            var data = JSON.parse($.parseJSON(data.d));
                if (data[0].Result == "Success") {
                    localStorage.setItem("DriverId", data[0].DriverId);
                    localStorage.setItem("DriverName", data[0].DriverName);
                    localStorage.setItem("VehicleId", data[0].VehicleId);
                    localStorage.setItem("VehicleRegNumber", data[0].VehicleRegNumber);
                    window.location = "dashboard.html";
                }
                else if (data[0].Result == "Error") {
                    alert(data[0].Message);
                    HideLoader();
                }
        }
    });
});

function LoadRoute() {
    var url1 = requestUrl + "/GetVehicleRoute";
    var VehicleId = localStorage.getItem("VehicleId");
    jQuery.ajax({
        url: url1,
        type: "POST",
        dataType: "json",
        data: "{'VehicleId':'" + VehicleId + "'}",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            var data = JSON.parse($.parseJSON(data.d));
            if (data[0].Result == "Success") {
                var myData = data[0].Message;
                var routeButton = "";
                for (var i = 0; i < myData.length; i++) {
                    routeButton += "<span style='font-size: 28px;' onclick='LoadStudentforPickupDrop(&#39;" + myData[i].RouteId + "&#39;);' class='waves-effect waves-light btn-large green width-100 m-b-20 animated bouncein delay-4'>" + myData[i].RouteName + "</span>";
                }
                $("#PnlRouteButtons").html(routeButton);
                $("#PnlRoute").show();
                HideLoader();
            }
            else if (data[0].Result == "Error") {
               
                HideLoader();
            }
        }
    });
}

function LoadStudentPickupDrop(type) {
    var routId = localStorage.getItem("RouteId");
    var url1 = requestUrl + "/LoadStudentPickupDrop";
    var VehicleId = localStorage.getItem("VehicleId");
    jQuery.ajax({
        url: url1,
        type: "POST",
        dataType: "json",
        data: "{'VehicleId':'" + VehicleId + "','Type':'" + type + "','RouteId':'" + routId + "'}",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            var data = JSON.parse($.parseJSON(data.d));
            if (data[0].Result == "Success") {
                $("#hfStudentId").val(data[0].StudentId);
                $("#lblStudentName").html(data[0].StudentName);
                $("#lblMobileNo").html(data[0].MobileNo);
                $("#lblFatherName").html(data[0].FatherName);
                $("#lblClassSection").html(data[0].Class);
                $("#lblAddress").html(data[0].Address);
                if (data[0].StudentPhoto != "") {
                    $("#studentPhoto").attr("src", siteDomain + data[0].StudentPhoto);
                }
                else {
                    $("#studentPhoto").attr("src", "img/icon-user-default.png");
                }
                HideLoader();
            }
            else if (data[0].Result == "Error") {
                //alert(data[0].Message);               
                if (data[0].IsReached == "True") {
                    $("#btnReachSchool").hide();
                }
                else if (data[0].IsReached == "False") {
                    $("#btnReachSchool").show();
                }
                $("#PnlStudentDetail").hide();
                $("#PnlBlank").show();
                $("#PnlBlankText").html(data[0].Message);
                
                HideLoader();
            }
        }
    });   
}

$("#btnAbsent").click(function () {
    if (confirm('Confirm.?')) {
        SavePickupDropTxn("False");
    }
});
$("#btnPickupDrop").click(function () {
    if (confirm('Confirm.?')) {
        SavePickupDropTxn("True");
    }
});

function SavePickupDropTxn(IsPresent){
    var _studentId = $("#hfStudentId").val();
    var _vehicleId = localStorage.getItem("VehicleId");
    var _routeId = localStorage.getItem("RouteId");
    var _type = $("#hfType").val();

    var url1 = requestUrl + "/SavePickupDropTxn";
    jQuery.ajax({
        url: url1,
        type: "POST",
        dataType: "json",
        data: "{'StudentId':'" + _studentId + "','VehicleId':'" + _vehicleId + "','IsPresent':'" + IsPresent + "','Type':'" + _type + "','RouteId':'" + _routeId + "'}",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            var data = JSON.parse($.parseJSON(data.d));
            if (data[0].Result == "Success") {
                LoadStudentPickupDrop(_type);
                HideLoader();
            }
            else if (data[0].Result == "Error") {
                alert(data[0].Message);
                HideLoader();
            }
        }
    });
   
}

function SaveSchoolReachTxn() {
    var _vehicleId = localStorage.getItem("VehicleId");
    var _routeId = localStorage.getItem("RouteId");
    var _type = $("#hfType").val();

    var url1 = requestUrl + "/SaveSchoolReachTxn";
    jQuery.ajax({
        url: url1,
        type: "POST",
        dataType: "json",
        data: "{'VehicleId':'" + _vehicleId + "','Type':'" + _type + "','RouteId':'" + _routeId + "'}",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            var data = JSON.parse($.parseJSON(data.d));
            if (data[0].Result == "Success") {
                $("#btnReachSchool").hide();
                $("#PnlStudentDetail").hide();
                $("#PnlBlank").show();
                $("#PnlBlankText").html(data[0].Message);
                HideLoader();
            }
            else if (data[0].Result == "Error") {
                alert(data[0].Message);
                HideLoader();
            }
        }
    });

}

function logoutsystem() {
    localStorage.removeItem("DriverId");
    window.location = "index.html";
}

  

    function ShowLoader() {
        $("#loader-wrapper").show();
       // $('body').removeClass('loaded');
    }

    function HideLoader() {
        $("#loader-wrapper").hide();
       // $('body').addClass('loaded');
    }

function getQueryStringByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
