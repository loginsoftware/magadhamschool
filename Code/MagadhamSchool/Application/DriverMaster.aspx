﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="DriverMaster.aspx.cs" Inherits="DriverMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-edit"></i>Driver Master</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <div role="form">
                    <div class="row form-group">
                    <div class="col-md-4">
                        <label>Driver Name</label>
    <asp:TextBox ID="TxtDriverName" runat="server" CssClass="form-control" placeholder="Enter Driver Name"></asp:TextBox>
                    </div>
                    <div class="col-md-4">
                        <label>Father Name</label>
    <asp:TextBox ID="TxtFatherName" runat="server" CssClass="form-control" placeholder="Enter Father Name"></asp:TextBox>
                    </div>
                    <div class="col-md-4">
                        <label>MobileNo</label>
    <asp:TextBox ID="TxtMobileNo" runat="server" CssClass="form-control" placeholder="Enter Mobile Number"></asp:TextBox>
                    </div>
                    <div class="col-md-4">
                        <label>Address</label>
    <asp:TextBox ID="TxtAddress" runat="server" CssClass="form-control" placeholder="Enter Address"></asp:TextBox>
                    </div>
                    <div class="col-md-4">
                        <label>UserName</label>
    <asp:TextBox ID="TxtUserName" runat="server" CssClass="form-control" placeholder="Enter UserName"></asp:TextBox>
                    </div>
                    <div class="col-md-4">
                        <label>Password</label>
    <asp:TextBox ID="TxtPassword" runat="server" CssClass="form-control" placeholder="Enter Password"></asp:TextBox>
                    </div>
                    <div class="col-md-4">
                        <label>IsActive</label>
    <asp:CheckBox ID="chkIsActive" runat="server" CssClass="form-control" Checked="true" />
                    </div>


                    </div>
                    

                    <div class="row">
                   <div class="col-md-4">
                    <asp:LinkButton ID="BtnSave" runat="server" CssClass="btn btn-default" OnClick="BtnSave_Click">Save</asp:LinkButton>
    <asp:HiddenField ID="hfPKey" runat="server" />
     </div>
     </div>
    
                </div>

            </div>
        </div>
    </div>
    <!--/span-->

</div>

<div class="row">
        <div class="box col-md-12">
            <div class="box-inner">
                <div class="box-header well" data-original-title="">
                    <h2><i class="glyphicon glyphicon-user"></i> Driver Detail</h2>

                    <div class="box-icon">
                        <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                        <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                    </div>
                </div>
                <div class="box-content">
                                  <asp:GridView ID="GVDetail" AutoGenerateColumns="False" AllowPaging="false" 
            runat="server" DataKeyNames="DriverId" EmptyDataText="No Record Found" 
            Width="100%" CssClass="table table-striped table-bordered responsive">
                            <AlternatingRowStyle BackColor="#ecebeb" />
                            <Columns>
                                 <asp:TemplateField HeaderText="Sr.No" HeaderStyle-Width="10px">
                            <ItemTemplate>
                                <asp:Label ID="lblSRNO" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Driver Name">
                            <ItemTemplate>
                                <asp:Label ID="lblDriverName" runat="server" Text='<%# Eval("DriverName") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="FatherName">
                            <ItemTemplate>
                                <asp:Label ID="lblDriverFatherName" runat="server" Text='<%# Eval("DriverFatherName") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Mobile">
                            <ItemTemplate>
                                <asp:Label ID="lblDriverMobileNo" runat="server" Text='<%# Eval("DriverMobileNo") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Address">
                            <ItemTemplate>
                                <asp:Label ID="lblDriverAddress" runat="server" Text='<%# Eval("DriverAddress") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="IsActive">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkIsActive" runat="server" Checked='<%# Eval("IsActive") %>' Enabled="false" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CreatedOn">
                            <ItemTemplate>
                                <asp:Label ID="lblCreatedOn" runat="server" Text='<%# Eval("CreatedOn", "{0:dd-MMM-yyyy}") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
    <asp:TemplateField HeaderText="Action">
                      <ItemTemplate>
                   <asp:LinkButton ID="LnkBtnEdit" CssClass="btn btn-success" runat="server" onclick="LnkBtnEdit_Click"
                            ToolTip="Click here to Edit">Edit</asp:LinkButton> 
                     <asp:LinkButton ID="LnkBtnDelete" CssClass="btn btn-danger"  runat="server" 
                      OnClientClick="return confirm('Are you sure to delete this record. ?') ;" 
                            ToolTip="Click here to Delete" onclick="LnkBtnDelete_Click">Delete</asp:LinkButton>       
                      </ItemTemplate>
                      </asp:TemplateField>
                            
                            </Columns>
                            </asp:GridView>


                    
                </div>
            </div>
        </div>
        <!--/span-->

    </div>
</asp:Content>

