﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;

public partial class Dashboard : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            TxtDate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
            FillShiftRadioButton(rbShift);
            rbShift.SelectedIndex = 0;
            LoadPickupDataList();
          

        }
    }

    private void LoadPickupDataList()
    {
        ShiftRouteMappingBL obj = new ShiftRouteMappingBL();
        DLDetail.DataSource = obj.LoadShiftRouteMappingByShiftId(int.Parse(rbShift.SelectedValue));
        DLDetail.DataBind();

        PickupTransactionBL objP = new PickupTransactionBL();
        SchoolReachTxnBL objR = new SchoolReachTxnBL();

        for (int i = 0; i < DLDetail.Items.Count; i++)
        {
            HiddenField hfVehicleId = (HiddenField)DLDetail.Items[i].FindControl("hfVehicleId");
            HiddenField hfRouteId = (HiddenField)DLDetail.Items[i].FindControl("hfRouteId");
            HyperLink lnkViewReport = (HyperLink)DLDetail.Items[i].FindControl("lnkViewReport");
            Label lblVehcileCurrentStatus = (Label)DLDetail.Items[i].FindControl("lblVehcileCurrentStatus");

            List<DataLayer.SchoolReachTxn> SR = objR.LoadSchoolReachTxnByCreatedOn(int.Parse(hfVehicleId.Value),int.Parse(hfRouteId.Value), DateTime.Parse(TxtDate.Text), "Pickup");
            if (SR.Count > 0)
            {
                lblVehcileCurrentStatus.Text = "Reached School at " + SR[0].ReachDatetime.ToString("dd-MMM-yyyy hh:mm tt") + "";
                lnkViewReport.NavigateUrl = "VehicleTxnDetail.aspx?vehicleid=" + hfVehicleId.Value + "&selecteddate=" + TxtDate.Text + "&routeid=" + hfRouteId.Value + "&txntype=" + rbTxnType.SelectedValue + "";
            }
            else
            {
                List<DataLayer.PickupTransactionPickupDateTime_Result> R = objP.LoadPickupTransactionPickupDateTime(DateTime.Parse(TxtDate.Text), int.Parse(hfRouteId.Value));
                if (R.Count > 0)
                {
                    DateTime d1 = DateTime.Parse(R[0].TodayPickupDate.ToString());
                    DateTime d2 = DateTime.Now;
                    try
                    {
                        d2 = DateTime.Parse(R[0].YesterdayPickupDate.ToString());
                    }
                    catch
                    {
                        d2 = DateTime.Parse(R[0].TodayPickupDate.ToString());
                    }
                    var result = d1 - d2;
                    double dResult = result.Minutes;
                    if (dResult > 0)
                    {
                        lblVehcileCurrentStatus.Text = "Before by " + dResult + " Min &nbsp;<br>&nbsp; on " + R[0].RouteName + " at " + R[0].StudentName + ", " + R[0].StudentAddress + "";
                    }
                    else if (dResult == 0)
                    {
                        lblVehcileCurrentStatus.Text = "RightTime at " + R[0].StudentName + ", " + R[0].StudentAddress + "";
                    }
                    else if (dResult < 0)
                    {
                        lblVehcileCurrentStatus.Text = "Delayed by " + (dResult * (-1)).ToString() + " Min &nbsp;<br>&nbsp; on " + R[0].RouteName + " at " + R[0].StudentName + ", " + R[0].StudentAddress + "";
                    }
                    lnkViewReport.NavigateUrl = "VehicleTxnDetail.aspx?vehicleid=" + hfVehicleId.Value + "&selecteddate=" + TxtDate.Text + "&routeid=" + hfRouteId.Value + "&txntype=" + rbTxnType.SelectedValue + "";
                }
            }            
        }
    }

    private void LoadDropDataList()
    {
        ShiftRouteMappingBL obj = new ShiftRouteMappingBL();
        DLDetail.DataSource = obj.LoadShiftRouteMappingByShiftId(int.Parse(rbShift.SelectedValue));
        DLDetail.DataBind();

        DropTransactionBL objP = new DropTransactionBL();
        SchoolReachTxnBL objR = new SchoolReachTxnBL();
        for (int i = 0; i < DLDetail.Items.Count; i++)
        {
            HiddenField hfVehicleId = (HiddenField)DLDetail.Items[i].FindControl("hfVehicleId");
            HiddenField hfRouteId = (HiddenField)DLDetail.Items[i].FindControl("hfRouteId");
            HyperLink lnkViewReport = (HyperLink)DLDetail.Items[i].FindControl("lnkViewReport");
            Label lblVehcileCurrentStatus = (Label)DLDetail.Items[i].FindControl("lblVehcileCurrentStatus");

            List<DataLayer.SchoolReachTxn> SR = objR.LoadSchoolReachTxnByCreatedOn(int.Parse(hfVehicleId.Value), int.Parse(hfRouteId.Value), DateTime.Parse(TxtDate.Text), "Drop");
            if (SR.Count > 0)
            {
                lblVehcileCurrentStatus.Text = "Reached School at " + SR[0].ReachDatetime.ToString("dd-MMM-yyyy hh:mm tt") + "";
                lnkViewReport.NavigateUrl = "VehicleTxnDetail.aspx?vehicleid=" + hfVehicleId.Value + "&selecteddate=" + TxtDate.Text + "&routeid=" + hfRouteId.Value + "&txntype=" + rbTxnType.SelectedValue + "";
            }
            else
            {
                List<DataLayer.DropTransactionDropDateTime_Result> R = objP.LoadDropTransactionDropDateTime(DateTime.Parse(TxtDate.Text), int.Parse(hfRouteId.Value));

                if (R.Count > 0)
                {
                    DateTime d1 = DateTime.Parse(R[0].TodayDropDate.ToString());
                    DateTime d2 = DateTime.Now;
                    try
                    {
                        d2 = DateTime.Parse(R[0].YesterdayDropDate.ToString());
                    }
                    catch
                    {
                        d2 = DateTime.Parse(R[0].TodayDropDate.ToString());
                    }
                    var result = d1 - d2;
                    double dResult = result.Minutes;
                    if (dResult > 0)
                    {
                        lblVehcileCurrentStatus.Text = "Before by " + dResult + " Min &nbsp;<br>&nbsp; on " + R[0].RouteName + " at " + R[0].StudentName + ", " + R[0].StudentAddress + "";
                    }
                    else if (dResult == 0)
                    {
                        lblVehcileCurrentStatus.Text = "RightTime at " + R[0].StudentName + ", " + R[0].StudentAddress + "";
                    }
                    else if (dResult < 0)
                    {
                        lblVehcileCurrentStatus.Text = "Delayed by " + (dResult * (-1)).ToString() + " Min &nbsp;<br>&nbsp; on " + R[0].RouteName + " at " + R[0].StudentName + ", " + R[0].StudentAddress + "";
                    }
                    lnkViewReport.NavigateUrl = "VehicleTxnDetail.aspx?vehicleid=" + hfVehicleId.Value + "&selecteddate=" + TxtDate.Text + "&routeid=" + hfRouteId.Value + "&txntype=" + rbTxnType.SelectedValue + "";
                }
            }
           
        }
    }
    protected void TxtDate_TextChanged(object sender, EventArgs e)
    {
        if (rbTxnType.SelectedValue == "Pickup")
        {
            LoadPickupDataList();
        }
        else
        {
            LoadDropDataList();
        }
    }
    protected void rbTxnType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rbTxnType.SelectedValue == "Pickup")
        {
            LoadPickupDataList();
        }
        else
        {
            LoadDropDataList();
        }
    }
    protected void rbShift_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rbTxnType.SelectedValue == "Pickup")
        {
            LoadPickupDataList();
        }
        else
        {
            LoadDropDataList();
        }
    }
}