﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;

public partial class Reciept_BusAllotment : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.QueryString["Id"] != null as string)
            {
                string allotmentId = DecryptString(Request.QueryString["Id"].ToString());
                LoadAllotmentDetail(allotmentId);
            }
        }
    }

    private void LoadAllotmentDetail(string allotmentId)
    {
        BusAllotmentBL obj = new BusAllotmentBL();
        List<DataLayer.VBusAllotment> D = obj.LoadBusAllotmentByAllotmentReciept(int.Parse(allotmentId));
        lblRecieptDate.Text = DateTime.Now.ToString("dd-MMM-yyyy hh:mm tt");
        lblSessionName.Text = GetCookie(this.Page, "SessionName");
        lblAllotmentCode.Text = D[0].AllotmentCode;
        lblStudentName.Text = D[0].StudentName + " " + D[0].ClassName + " " + D[0].SectionName;
        lblFatherName.Text = D[0].FatherName;
        lblMobileNo.Text = D[0].MobileNo;
        lblAddress.Text = D[0].Address;
        //------------------------------
        lblRecieptDate1.Text = DateTime.Now.ToString("dd-MMM-yyyy hh:mm tt");
        lblSessionName1.Text = GetCookie(this.Page, "SessionName");
        lblAllotmentCode1.Text = D[0].AllotmentCode;
        lblStudentName1.Text = D[0].StudentName + " " + D[0].ClassName + " " + D[0].SectionName;
        lblFatherName1.Text = D[0].FatherName;
        lblMobileNo1.Text = D[0].MobileNo;
        lblAddress1.Text = D[0].Address;

        VehicleMasterBL objV = new VehicleMasterBL();
        List<DataLayer.VehicleMaster> V = objV.LoadVehicleMasterByPrimaryKey(int.Parse(D[0].VehicleId.ToString()));
        lblVehicleRegNo.Text = V[0].VehicleCode + "-" + V[0].VehicleRegNumber;
        //------------------------------------------------------------------
        lblVehicleRegNo1.Text = V[0].VehicleCode + "-" + V[0].VehicleRegNumber;

        DriverMasterBL objD = new DriverMasterBL();
        List<DataLayer.DriverMaster> Dr = objD.LoadDriverMasterByPrimaryKey(V[0].DriverId);
        lblDriverName.Text = Dr[0].DriverName;
        lblDriverMobile.Text = Dr[0].DriverMobileNo;
        //-----------------------------------------------
        lblDriverName1.Text = Dr[0].DriverName;
        lblDriverMobile1.Text = Dr[0].DriverMobileNo;

        RoutMasterBL objR = new RoutMasterBL();
        List<DataLayer.RoutMaster> R = objR.LoadRoutMasterByPrimaryKey(int.Parse(D[0].RouteId.ToString()));
        lblRouteName.Text = R[0].RouteName;
        //------------------------------------------
        lblRouteName1.Text = R[0].RouteName;
        
    }
}