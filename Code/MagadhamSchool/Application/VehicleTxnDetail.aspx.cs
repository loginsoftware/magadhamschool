﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;

public partial class VehicleTxnDetail : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            FillVehicle(DdnVehicle);
            if (Request.QueryString["vehicleid"] != null as string)
            {
                DdnVehicle.SelectedValue = Request.QueryString["vehicleid"].ToString();
                FillRoute(DdnRoute, int.Parse(DdnVehicle.SelectedValue));
                DdnRoute.SelectedValue = Request.QueryString["routeid"].ToString();
                TxtDate.Text = Request.QueryString["selecteddate"].ToString();
                rbTxnType.SelectedValue = Request.QueryString["txntype"].ToString();
                
                LoadVehicleTxn(int.Parse(DdnVehicle.SelectedValue), int.Parse(DdnRoute.SelectedValue), rbTxnType.SelectedValue);

            }
        }
    }

    protected void DdnVehicle_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DdnVehicle.SelectedIndex > 0)
        {
            FillRoute(DdnRoute, int.Parse(DdnVehicle.SelectedValue));
        }
    }

    protected void BtnShow_Click(object sender, EventArgs e)
    {
        try
        {
            LoadVehicleTxn(int.Parse(DdnVehicle.SelectedValue), int.Parse(DdnRoute.SelectedValue), rbTxnType.SelectedValue);
        }
        catch { }
    }

    private void LoadVehicleTxn(int _vehicleId, int _routeId, string _txnType)
    {
        try
        {
            string _sessionId = GetCookie(this.Page, "SessionId");
            DateTime _todayDate = DateTime.Parse(TxtDate.Text);
            DateTime _yesterdayDate = DateTime.Parse(TxtDate.Text).AddDays(-1);
            DateTime _dayBeforeYesterdayDate = DateTime.Parse(TxtDate.Text).AddDays(-2);

            VehicleMasterBL objD = new VehicleMasterBL();
            List<DataLayer.VVehicleMaster> D = objD.LoadVehicleMasterByVehicledV(_vehicleId);
            lblVehicleDriverName.Text = D[0].VehicleCode + "/" + D[0].VehicleRegNumber + "/" + D[0].DriverName + "/" + D[0].DriverMobileNo;

            ////////////////////////////////////////////////////////-----report
            BusAllotmentBL obj = new BusAllotmentBL();
            List<DataLayer.BusAllotmentPickupDropTxn_Result> BA = obj.LoadBusAllotmentPickupDropTxn(_vehicleId, int.Parse(_sessionId), _todayDate, _yesterdayDate, _dayBeforeYesterdayDate);
            GVDetail.DataSource = BA;
            GVDetail.DataBind();

            Label lblTodayDate = (Label)GVDetail.HeaderRow.FindControl("lblTodayDate");
            Label lblYesterdayDate = (Label)GVDetail.HeaderRow.FindControl("lblYesterdayDate");
            Label lblDayBeforeYesterdayDate = (Label)GVDetail.HeaderRow.FindControl("lblDayBeforeYesterdayDate");

            lblTodayDate.Text = _todayDate.ToString("dd-MMM-yyyy");
            lblYesterdayDate.Text = _yesterdayDate.ToString("dd-MMM-yyyy");
            lblDayBeforeYesterdayDate.Text = _dayBeforeYesterdayDate.ToString("dd-MMM-yyyy");

            for (int i = 0; i < GVDetail.Rows.Count; i++)
            {
                if (_txnType == "Pickup")
                {
                    Label lblTodaysPickup = (Label)GVDetail.Rows[i].FindControl("lblTodaysPickup");
                    Label lblYesterdayPickup = (Label)GVDetail.Rows[i].FindControl("lblYesterdayPickup");
                    Label lblDayBeforeYesterdayPickup = (Label)GVDetail.Rows[i].FindControl("lblDayBeforeYesterdayPickup");
                    lblTodaysPickup.Visible = false;
                    lblYesterdayPickup.Visible = false;
                    lblDayBeforeYesterdayPickup.Visible = false;
                }
                else if (_txnType == "Drop")
                {
                    Label lblTodaysDrop = (Label)GVDetail.Rows[i].FindControl("lblTodaysDrop");
                    Label lblYesterdayDrop = (Label)GVDetail.Rows[i].FindControl("lblYesterdayDrop");
                    Label lblDayBeforeYesterdayDrop = (Label)GVDetail.Rows[i].FindControl("lblDayBeforeYesterdayDrop");
                    lblTodaysDrop.Visible = false;
                    lblYesterdayDrop.Visible = false;
                    lblDayBeforeYesterdayDrop.Visible = false;
                }
            }
        }
        catch { }
    }


}