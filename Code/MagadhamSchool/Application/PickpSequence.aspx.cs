﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;

public partial class PickpSequence : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            FillVehicle(DdnVehicle);
        }
    }

    protected void BtnShow_Click(object sender, EventArgs e)
    {
        if (DdnVehicle.SelectedIndex > 0 && DdnRoute.SelectedIndex > 0)
        {
            int _sessionId = int.Parse(GetCookie(this.Page, "SessionId"));
            int _vehicleId = int.Parse(DdnVehicle.SelectedValue);
            int _routeId = int.Parse(DdnRoute.SelectedValue);
            StudentMasterBL obj = new StudentMasterBL();
            GVDetail.DataSource = obj.LoadStudentMasterByVehicleIdRouteId(_vehicleId, _routeId, _sessionId);
            GVDetail.DataBind();

            lnkUpdateDisplayOrder.Visible = true;
        }
    }

    protected void lnkUpdateDisplayOrder_Click(object sender, EventArgs e)
    {
        int _sessionId = int.Parse(GetCookie(this.Page, "SessionId"));
        for (int i = 0; i < GVDetail.Rows.Count; i++)
        {
            HiddenField hfAllotmentId = (HiddenField)GVDetail.Rows[i].FindControl("hfAllotmentId");
            TextBox TxtDisplayOrder = (TextBox)GVDetail.Rows[i].FindControl("TxtDisplayOrder");

            BusAllotmentBL obj = new BusAllotmentBL();

           obj.UpdateBusAllotmentDisplayOrder(int.Parse(hfAllotmentId.Value), int.Parse(TxtDisplayOrder.Text));
            
        }

        ShowMessage("Update Successfully", this.Page);
    }

    protected void DdnVehicle_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DdnVehicle.SelectedIndex > 0)
        {
            FillRoute(DdnRoute, int.Parse(DdnVehicle.SelectedValue));
        }
    }
}