﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;

public partial class SectionMaster : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.QueryString["Id"] != null as string)
            {
                hfPKey.Value = DecryptString(Request.QueryString["Id"].ToString());
                LoadDataFromDatabase(int.Parse(hfPKey.Value));
                BtnSave.Text = "Update";
            }
            if (Request.QueryString["result"] != null as string)
            {
                string _result = Request.QueryString["result"].ToString();
                if (_result == "save") ShowMessage("Record Saved Successfully", this.Page);
                else if (_result == "update") ShowMessage("Record Updated Successfully", this.Page);
            }
            LoadDataFromDatabase();
        }
    }

    private void LoadDataFromDatabase()
    {
        SectionMasterBL obj = new SectionMasterBL();
        GVDetail.DataSource = obj.LoadAllSectionMaster();
        GVDetail.DataBind();
    }

    private void LoadDataFromDatabase(int Pkey)
    {
        SectionMasterBL obj = new SectionMasterBL();
        List<DataLayer.SectionMaster> S = obj.LoadSectionMasterByPrimaryKey(Pkey);
        TxtSectionName.Text = S[0].SectionName;
    }

    protected void LnkBtnDelete_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton LnkBtn = (LinkButton)sender;
            GridViewRow gvRow = (GridViewRow)LnkBtn.NamingContainer;
            string Pkey = GVDetail.DataKeys[gvRow.RowIndex].Values[0].ToString();
            int rowIndex = gvRow.RowIndex;
            SectionMasterBL objL = new SectionMasterBL();
            int result = objL.DeleteSectionMaster(int.Parse(Pkey));

            if (result != null)
            {
                ShowMessage("Delete Success", this.Page);
                LoadDataFromDatabase();
            }

        }
        catch (Exception Ex)
        {
            ShowMessage(Ex.InnerException.Message, this.Page);
        }
    }

    protected void LnkBtnEdit_Click(object sender, EventArgs e)
    {
        LinkButton LnkBtn = (LinkButton)sender;
        GridViewRow gvRow = (GridViewRow)LnkBtn.NamingContainer;
        string Pkey = GVDetail.DataKeys[gvRow.RowIndex].Values[0].ToString();
        Response.Redirect("SectionMaster.aspx?Id=" + EncryptString(Pkey));
    }

    protected void BtnSave_Click(object sender, EventArgs e)
    {
        if (!ChkAll()) return;

        if (BtnSave.Text == "Save")
        {
            SaveOrUpdate("Save");
        }
        else
        {
            SaveOrUpdate("Edit");
        }
    }

    private void SaveOrUpdate(string Action)
    {
        try
        {
            SectionMasterBL Obj = new SectionMasterBL();
            if (Action == "Save")
            {
                int _result = Obj.InsertSectionMaster(TxtSectionName.Text);

                if (_result != null)
                {
                    Response.Redirect("SectionMaster.aspx?result=save", false);
                }
            }
            else
            {
                int _result = Obj.UpdateSectionMaster(int.Parse(hfPKey.Value), TxtSectionName.Text);
                Response.Redirect("SectionMaster.aspx?result=update", false);

            }
        }

        catch (Exception Ex)
        {
            ShowMessage(Ex.Message, this.Page);
        }
    }

    private bool ChkAll()
    {
        if (TxtSectionName.Text == "")
        {
            ShowMessage("Enter Section Name", this.Page);
            return false;
        }

        return true;
    }
}