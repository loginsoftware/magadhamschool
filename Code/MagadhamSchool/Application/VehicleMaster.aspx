﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="VehicleMaster.aspx.cs" Inherits="VehicleMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-edit"></i>Vehicle Master</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <div role="form">
                    <div class="row form-group">
                    <div class="col-md-4">
                        <label>Vehicle Code</label>
    <asp:TextBox ID="TxtVehicleCode" runat="server" CssClass="form-control" placeholder="Enter Vehicle Code"></asp:TextBox>
                    </div>
                    <div class="col-md-4">
                        <label>Vehicle Type</label><br />
                        <asp:DropDownList ID="DdnVehicleType" runat="server" Width="100%" Height="30px">
                        <asp:ListItem Value="Van">Van</asp:ListItem>
                        <asp:ListItem Value="Bus">Bus</asp:ListItem>
                        <asp:ListItem Value="Auto">Auto</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-4">
                        <label>Vehicle Reg No.</label>
    <asp:TextBox ID="TxtVehicleRegNumber" style="text-transform:uppercase;" runat="server" CssClass="form-control" placeholder="Enter Vehicle Reg Number"></asp:TextBox>
                    </div>
                    <div class="col-md-4">
                        <label>TotalSeat</label>
    <asp:TextBox ID="TxtTotalSeat" runat="server" CssClass="form-control" placeholder="Enter Total Seat"></asp:TextBox>
                    </div>
                    <div class="col-md-4">
                        <label>Driver</label><br />
                        <asp:DropDownList ID="DdnDriver" runat="server" Width="100%" Height="30px" AppendDataBoundItems="true">
                        <asp:ListItem>--Select Driver--</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-4">
                        <label>IsActive</label>
    <asp:CheckBox ID="chkIsActive" runat="server" CssClass="form-control" Checked="true" />
                    </div>


                    </div>
                    

                    <div class="row">
                   <div class="col-md-4">
                    <asp:LinkButton ID="BtnSave" runat="server" CssClass="btn btn-default" OnClick="BtnSave_Click">Save</asp:LinkButton>
    <asp:HiddenField ID="hfPKey" runat="server" />
     </div>
     </div>
    
                </div>

            </div>
        </div>
    </div>
    <!--/span-->

</div>

<div class="row">
        <div class="box col-md-12">
            <div class="box-inner">
                <div class="box-header well" data-original-title="">
                    <h2><i class="glyphicon glyphicon-user"></i> Vehicle Detail</h2>

                    <div class="box-icon">
                        <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                        <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                    </div>
                </div>
                <div class="box-content">
                                  <asp:GridView ID="GVDetail" AutoGenerateColumns="False" AllowPaging="false" 
            runat="server" DataKeyNames="VehicleId" EmptyDataText="No Record Found" 
            Width="100%" CssClass="table table-striped table-bordered responsive">
                            <AlternatingRowStyle BackColor="#ecebeb" />
                            <Columns>
                                 <asp:TemplateField HeaderText="Sr.No" HeaderStyle-Width="10px">
                            <ItemTemplate>
                                <asp:Label ID="lblSRNO" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Vehicle Code">
                            <ItemTemplate>
                                <asp:Label ID="lblVehicleCode" runat="server" Text='<%# Eval("VehicleCode") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Type">
                            <ItemTemplate>
                                <asp:Label ID="lblVehicleType" runat="server" Text='<%# Eval("VehicleType") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Registration No.">
                            <ItemTemplate>
                                <asp:Label ID="lblVehicleRegNumber" runat="server" Text='<%# Eval("VehicleRegNumber") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>                            
                             <asp:TemplateField HeaderText="TotalSeat">
                            <ItemTemplate>
                                <asp:Label ID="lblTotalSeat" runat="server" Text='<%# Eval("TotalSeat") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Driver">
                            <ItemTemplate>
                                <asp:Label ID="lblDriverName" runat="server" Text='<%# Eval("DriverName") %>'></asp:Label><br />
                                <asp:Label ID="lblDriverMobileNo" runat="server" Text='<%# Eval("DriverMobileNo") %>'></asp:Label>

                            </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="IsActive">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkIsActive" runat="server" Checked='<%# Eval("IsActive") %>' Enabled="false" />
                            </ItemTemplate>
                            </asp:TemplateField>
                           
    <asp:TemplateField HeaderText="Action">
                      <ItemTemplate>
                   <asp:LinkButton ID="LnkBtnEdit" CssClass="btn btn-success" runat="server" onclick="LnkBtnEdit_Click"
                            ToolTip="Click here to Edit">Edit</asp:LinkButton> 
                     <asp:LinkButton ID="LnkBtnDelete" CssClass="btn btn-danger"  runat="server" 
                      OnClientClick="return confirm('Are you sure to delete this record. ?') ;" 
                            ToolTip="Click here to Delete" onclick="LnkBtnDelete_Click">Delete</asp:LinkButton>       
                      </ItemTemplate>
                      </asp:TemplateField>
                            
                            </Columns>
                            </asp:GridView>


                    
                </div>
            </div>
        </div>
        <!--/span-->

    </div>
</asp:Content>

