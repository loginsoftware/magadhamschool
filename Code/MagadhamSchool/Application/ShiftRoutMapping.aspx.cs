﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;

public partial class ShiftRoutMapping : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            FillShift(DdnShift);
            FillRoute(DdnRoute);
            if (Request.QueryString["Id"] != null as string)
            {
                hfPKey.Value = DecryptString(Request.QueryString["Id"].ToString());
                LoadDataFromDatabase(int.Parse(hfPKey.Value));
                BtnSave.Text = "Update";
            }
            if (Request.QueryString["result"] != null as string)
            {
                string _result = Request.QueryString["result"].ToString();
                if (_result == "save") ShowMessage("Record Saved Successfully", this.Page);
                else if (_result == "update") ShowMessage("Record Updated Successfully", this.Page);
            }
            LoadDataFromDatabase();
        }
    }

    private void LoadDataFromDatabase()
    {
        ShiftRouteMappingBL obj = new ShiftRouteMappingBL();
        GVDetail.DataSource = obj.LoadAllShiftRouteMappingV();
        GVDetail.DataBind();
    }

    private void LoadDataFromDatabase(int Pkey)
    {
        ShiftRouteMappingBL obj = new ShiftRouteMappingBL();
        List<DataLayer.ShiftRouteMapping> S = obj.LoadShiftRouteMappingByPrimaryKey(Pkey);
        DdnShift.SelectedValue = S[0].ShiftId.ToString();
        DdnRoute.SelectedValue = S[0].RouteId.ToString();
    }

    protected void LnkBtnDelete_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton LnkBtn = (LinkButton)sender;
            GridViewRow gvRow = (GridViewRow)LnkBtn.NamingContainer;
            string Pkey = GVDetail.DataKeys[gvRow.RowIndex].Values[0].ToString();
            int rowIndex = gvRow.RowIndex;
            ShiftRouteMappingBL objL = new ShiftRouteMappingBL();
            int result = objL.DeleteShiftRouteMapping(int.Parse(Pkey));

            if (result != null)
            {
                ShowMessage("Delete Success", this.Page);
                LoadDataFromDatabase();
            }

        }
        catch (Exception Ex)
        {
            ShowMessage(Ex.InnerException.Message, this.Page);
        }
    }

    protected void LnkBtnEdit_Click(object sender, EventArgs e)
    {
        LinkButton LnkBtn = (LinkButton)sender;
        GridViewRow gvRow = (GridViewRow)LnkBtn.NamingContainer;
        string Pkey = GVDetail.DataKeys[gvRow.RowIndex].Values[0].ToString();
        Response.Redirect("ShiftRoutMapping.aspx?Id=" + EncryptString(Pkey));
    }

    protected void BtnSave_Click(object sender, EventArgs e)
    {
        if (!ChkAll()) return;

        if (BtnSave.Text == "Save")
        {
            SaveOrUpdate("Save");
        }
        else
        {
            SaveOrUpdate("Edit");
        }
    }

    private void SaveOrUpdate(string Action)
    {
        try
        {
            ShiftRouteMappingBL Obj = new ShiftRouteMappingBL();
            if (Action == "Save")
            {
                int _result = Obj.InsertShiftRouteMapping(int.Parse(DdnShift.SelectedValue), int.Parse(DdnRoute.SelectedValue));

                if (_result != null)
                {
                    Response.Redirect("ShiftRoutMapping.aspx?result=save", false);
                }
            }
            else
            {
                int _result = Obj.UpdateShiftRouteMapping(int.Parse(hfPKey.Value), int.Parse(DdnShift.SelectedValue), int.Parse(DdnRoute.SelectedValue));
                Response.Redirect("ShiftRoutMapping.aspx?result=update", false);

            }
        }

        catch (Exception Ex)
        {
            if (Ex.InnerException.Message.ToLower().Contains("uk_shiftroutemapping"))
            {
                ShowMessage(DdnRoute.SelectedItem.Text + " already asigned in " + DdnShift.SelectedItem.Text, this.Page);
            }
        }
    }

    private bool ChkAll()
    {
        if (DdnShift.SelectedIndex == 0 || DdnRoute.SelectedIndex == 0)
        {
            ShowMessage("Select Shift and Route.", this.Page);
            return false;
        }

        return true;
    }
}