﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Dashboard.aspx.cs" Inherits="Dashboard" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-edit"></i>Todays Pickup Drop Detail</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <div role="form">
                    <div class="row form-group">
                    <div class="col-md-3">
                        <label>Date:</label>
    <asp:TextBox ID="TxtDate" runat="server" CssClass="form-control" 
                            ontextchanged="TxtDate_TextChanged" AutoPostBack="true"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender1" TargetControlID="TxtDate" PopupButtonID="TxtDate" runat="server" Format="dd-MMM-yyyy">
                        </asp:CalendarExtender>
                        </div>
                         <div class="col-md-3">
                             <br />
                             <asp:RadioButtonList ID="rbTxnType" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="rbTxnType_SelectedIndexChanged">
                                 <asp:ListItem Value="Pickup" Selected="True">Pickup</asp:ListItem>
                                 <asp:ListItem Value="Drop">Drop</asp:ListItem>
                             </asp:RadioButtonList>
                        </div>
                         <div class="col-md-6">
                             <label>Shift:</label>
                             <asp:RadioButtonList ID="rbShift" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="rbShift_SelectedIndexChanged">
                                 
                             </asp:RadioButtonList>
                        </div>                        
                    </div>

                    <div class="row form-group">
                     <div class="col-md-12">
                         <asp:ListView ID="DLDetail" runat="server">
                            <ItemTemplate>
                               
                                 <div class="col-md-4">
                                      <asp:HiddenField ID="hfVehicleId" runat="server" Value='<%# Eval("VehicleId") %>' />
                             <asp:HiddenField ID="hfRouteId" runat="server" Value='<%# Eval("RouteId") %>' />

                         <table style="cursor:pointer;">
                         <tr>
                             <td style="text-align:center;"><span class="label-info label label-default">
                                 <%# Eval("VehicleCode") %>-<%# Eval("RouteName") %>

                                                            </span></td>

                         </tr>
                         <tr><td style="text-align: center;">
                             <asp:HyperLink ID="lnkViewReport" runat="server"><img src="img/schoolbus.png" /></asp:HyperLink> </td></tr>
                          <tr><td style="text-align:center;"><span class="label-info label label-warning"><%# Eval("VehicleRegNumber")%></span></td></tr>
                           <tr><td style="text-align:center;font-size:16px;"><span class="label label-success">
                               <asp:Label ID="lblVehcileCurrentStatus" runat="server" Text="" style="line-height: 1.8;"></asp:Label>

                            </span></td></tr>
                         </table>
                                 </div>
                            </ItemTemplate>
                         </asp:ListView>
                         <%--<asp:DataList ID="DLDetail" runat="server" RepeatDirection="Horizontal" RepeatColumns="3" RepeatLayout="Flow">
                         <ItemTemplate>
                             <asp:HiddenField ID="hfVehicleId" runat="server" Value='<%# Eval("VehicleId") %>' />
                             <asp:HiddenField ID="hfRouteId" runat="server" Value='<%# Eval("RouteId") %>' />

                         <table style="cursor:pointer;">
                         <tr>
                             <td style="text-align:center;"><span class="label-info label label-default">
                                 <%# Eval("VehicleCode") %>-<%# Eval("RouteName") %>

                                                            </span></td>

                         </tr>
                         <tr><td style="text-align: center;">
                             <asp:HyperLink ID="lnkViewReport" runat="server"><img src="img/schoolbus.png" /></asp:HyperLink> </td></tr>
                          <tr><td style="text-align:center;"><span class="label-info label label-warning"><%# Eval("VehicleRegNumber")%></span></td></tr>
                           <tr><td style="text-align:center;font-size:16px;"><span class="label label-success">
                               <asp:Label ID="lblVehcileCurrentStatus" runat="server" Text="" style="line-height: 1.8;"></asp:Label>

                            </span></td></tr>
                         </table>
                            
                         </ItemTemplate>
                         </asp:DataList>--%>
                     </div>
                    </div>




                </div>

            </div>
        </div>
    </div>
    <!--/span-->

</div>
            </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

