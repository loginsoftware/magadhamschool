﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;

public partial class Report_Attendance : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            TxtDate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
            FillVehicle(DdnVehicle);
        }
    }

    protected void DdnVehicle_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DdnVehicle.SelectedIndex > 0)
        {
            FillRoute(DdnRoute, int.Parse(DdnVehicle.SelectedValue));
        }
    }

    protected void BtnShow_Click(object sender, EventArgs e)
    {
        if (DdnVehicle.SelectedIndex > 0 && DdnRoute.SelectedIndex > 0)
        {
            LoadDataFromDatabase();
        }
        else
        {

        }
        
    }

    private void LoadDataFromDatabase()
    {
        PickupTransactionBL obj = new PickupTransactionBL();
        List<DataLayer.VPickupTransaction> P = obj.LoadPickupTransactionByCreatedOn(int.Parse(DdnVehicle.SelectedValue), int.Parse(DdnRoute.SelectedValue), DateTime.Parse(TxtDate.Text));
        if (P.Count > 0)
        {
            if (rbIsPresent.SelectedIndex == 0)
            {
                P = P.Where(f => f.IsPresent == true).ToList();
            }
            else if (rbIsPresent.SelectedIndex == 1) 
            {
                P = P.Where(f => f.IsPresent == false).ToList();
               
            }

            GVDetail.DataSource = P;
            GVDetail.DataBind();

            lblVehicleRoute.Text = "Vehcile: " + P[0].VehicleCode + "-" + P[0].VehicleRegNumber + " Route: " + P[0].RouteName + "";
        }
        else
        {
            ShowMessage("No Record Found for Select criteria", this.Page);
        }
    }
}