﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Site : System.Web.UI.MasterPage
{
    Utility Ut = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            if (Ut.GetCookie(this.Page, "LoginId") == null as string || Ut.GetCookie(this.Page, "LoginId") == "")
            {
                Response.Redirect("Default.aspx", false);
            }
            else
            {
                lblSessionName.Text = Ut.GetCookie(this.Page, "SessionName");
                lblUserDisplayName.Text = Ut.GetCookie(this.Page, "DisplayName");
            }
            
        }
    }
}
