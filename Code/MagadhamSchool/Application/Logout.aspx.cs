﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Logout : System.Web.UI.Page
{
    Utility Ut = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        Ut.SetCookie(this.Page, "UserId", "");
        Response.Redirect("Default.aspx", false);
    }
}