﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;
using System.IO;

public partial class StudentMaster  : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            FillClass(DdnClass);
            FillSection(DdnSection);
            FillVehicle(DdnVehicle);
            if (Request.QueryString["Id"] != null as string)
            {
                hfPKey.Value = DecryptString(Request.QueryString["Id"].ToString());
                hfAllotmentId.Value = DecryptString(Request.QueryString["allotmentid"].ToString());
                LoadDataFromDatabase(int.Parse(hfPKey.Value), int.Parse(hfAllotmentId.Value));
                BtnSave.Text = "Update";
            }
            if (Request.QueryString["result"] != null as string)
            {
                string _result = Request.QueryString["result"].ToString();
                if (_result == "save") ShowMessage("Record Saved Successfully", this.Page);
                else if (_result == "update") ShowMessage("Record Updated Successfully", this.Page);
                //------ code for print invoice on new window -----------
                if (Request.QueryString["print_reciept"] != null as string)
                {
                    string allotId = Request.QueryString["print_reciept"].ToString();
                    string url = "Reciept_BusAllotment.aspx?Id=" + allotId;
                    string s = "window.open('" + url + "', 'popup_window', 'width=1024,height=auto,left=0,top=0,resizable=yes');";
                    ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
                }
            }
           
        }
    }

   
    private void LoadDataFromDatabase(int Pkey, int allotmentId)
    {
        StudentMasterBL obj = new StudentMasterBL();
        List<DataLayer.StudentMaster> S = obj.LoadStudentMasterByPrimaryKey(Pkey);
        TxtStudentName.Text = S[0].StudentName;
        TxtFatherName.Text = S[0].FatherName;
        TxtMobileNo.Text = S[0].MobileNo;
        TxtAddress.Text = S[0].Address;
        TxtAdmissionNo.Text = S[0].AdmissionNo;
        DdnClass.SelectedValue = S[0].ClassId.ToString();
        DdnSection.SelectedValue = S[0].SectionId.ToString();
        chkIsActive.Checked = S[0].IsActive;
        hfStudentPhoto.Value = S[0].StudentPhoto;
        ImgStudentPhoto.ImageUrl = S[0].StudentPhoto;

        BusAllotmentBL objA = new BusAllotmentBL();
        List<DataLayer.BusAllotment> B = objA.LoadBusAllotmentByPrimaryKey(allotmentId);
        hfAllotmentCode.Value = B[0].AllotmentCode;
        hfDisplayOrder.Value = B[0].DisplayOrder.ToString();

        DdnVehicle.SelectedValue = B[0].VehicleId.ToString();
        if (B[0].VehicleId != null)
        {
            FillRoute(DdnRoute, int.Parse(B[0].VehicleId.ToString()));
            DdnRoute.SelectedValue = B[0].RouteId.ToString();
        }
      
        
        
    }

   
    protected void BtnSave_Click(object sender, EventArgs e)
    {
        if (!ChkAll()) return;

        if (BtnSave.Text == "Save")
        {
            SaveOrUpdate("Save");
        }
        else
        {
            SaveOrUpdate("Edit");
        }
    }

    private void SaveOrUpdate(string Action)
    {
        try
        {
            SaveImage();
            int _sessionId = int.Parse(GetCookie(this.Page, "SessionId"));
            StudentMasterBL Obj = new StudentMasterBL();
            if (Action == "Save")
            {
                int _studentId = Obj.InsertStudentMaster(TxtStudentName.Text, TxtFatherName.Text, TxtMobileNo.Text, TxtAdmissionNo.Text, hfStudentPhoto.Value, int.Parse(DdnClass.SelectedValue), int.Parse(DdnSection.SelectedValue), TxtAddress.Text, DateTime.Now, chkIsActive.Checked);

                int? _vehicleId = null;
                int? _routeId = null;
                if (DdnVehicle.SelectedIndex > 0) _vehicleId = int.Parse(DdnVehicle.SelectedValue);
                if (DdnRoute.SelectedIndex > 0) _routeId = int.Parse(DdnRoute.SelectedValue);
                BusAllotmentBL objB = new BusAllotmentBL();
                int _allotId = objB.InsertBusAllotment("", _studentId, _vehicleId, _routeId, 1, _sessionId, DateTime.Now);
                objB.UpdateBusAllotmentAllotmentCode(_allotId, "MIS-" + _studentId + "-" + _allotId);


                if (DdnVehicle.SelectedIndex > 0 && DdnRoute.SelectedIndex > 0)
                {
                    Response.Redirect("StudentMaster.aspx?result=update&print_reciept=" + EncryptString(_allotId.ToString()) + "", false);
                }
                else
                {
                    Response.Redirect("StudentMaster.aspx?result=save", false);
                }
                
            }
            else
            {
                int _studentId = Obj.UpdateStudentMaster(int.Parse(hfPKey.Value), TxtStudentName.Text, TxtFatherName.Text, TxtMobileNo.Text, TxtAdmissionNo.Text, hfStudentPhoto.Value, int.Parse(DdnClass.SelectedValue), int.Parse(DdnSection.SelectedValue), TxtAddress.Text, DateTime.Now, chkIsActive.Checked);

                int? _vehicleId = null;
                int? _routeId = null;
                if (DdnVehicle.SelectedIndex > 0) _vehicleId = int.Parse(DdnVehicle.SelectedValue);
                if (DdnRoute.SelectedIndex > 0) _routeId = int.Parse(DdnRoute.SelectedValue);
                if (DdnVehicle.SelectedIndex == 0 || DdnRoute.SelectedIndex == 0)
                {
                    _vehicleId = null;
                    _routeId = null;
                }
                BusAllotmentBL objB = new BusAllotmentBL();
                int _allotId = objB.UpdateBusAllotment(int.Parse(hfAllotmentId.Value), hfAllotmentCode.Value, _studentId, _vehicleId, _routeId, int.Parse(hfDisplayOrder.Value), _sessionId, DateTime.Now);

                if (DdnVehicle.SelectedIndex > 0 && DdnRoute.SelectedIndex > 0)
                {
                    Response.Redirect("StudentMaster.aspx?result=update&print_reciept=" + EncryptString(_allotId.ToString()) + "", false);
                }
                else
                {
                    Response.Redirect("StudentMaster.aspx?result=update", false);
                }

            }
        }

        catch (Exception Ex)
        {
            ShowMessage(Ex.Message, this.Page);
        }
    }

    private void SaveImage()
    {
        if (FUStudentPhoto.HasFile)
        {
            string FileName = Path.GetFileName(FUStudentPhoto.PostedFile.FileName);
            string FileExt = Path.GetExtension(FUStudentPhoto.PostedFile.FileName);
            FileExt = FileExt.ToLower();
            FileName = GenerateRandomString() + DateTime.Now.ToString("ddMMMyyyyhhmmssffffff") + FileExt;

            if (FileExt == ".jpg" || FileExt == ".jpeg" || FileExt == ".png")
            {
                FileName = CheckFileNameExist("StudentPhoto", FileName);
                FUStudentPhoto.SaveAs(Server.MapPath("~//StudentPhoto//" + FileName));
                hfStudentPhoto.Value = "StudentPhoto/" + FileName;
            }
        }
    }

    private bool ChkAll()
    {
        if (TxtStudentName.Text == "")
        {
            ShowMessage("Enter Student Name", this.Page);
            return false;
        }
        if (TxtMobileNo.Text == "")
        {
            ShowMessage("Enter Mobile No", this.Page);
            return false;
        }
        if (TxtAdmissionNo.Text == "")
        {
            ShowMessage("Enter Admission No", this.Page);
            return false;
        }
        if (TxtAddress.Text == "")
        {
            ShowMessage("Enter Address", this.Page);
            return false;
        }
        if (DdnClass.SelectedIndex == 0)
        {
            ShowMessage("Select Class", this.Page);
            return false;
        }
        if (DdnSection.SelectedIndex == 0)
        {
            ShowMessage("Select Section", this.Page);
            return false;
        }
        
        return true;
    }
    protected void DdnVehicle_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DdnVehicle.SelectedIndex > 0)
        {
            FillRoute(DdnRoute, int.Parse(DdnVehicle.SelectedValue));
        }
    }
}