﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;

public partial class RouteMaster : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            FillVehicle(DdnVehicle);
            if (Request.QueryString["Id"] != null as string)
            {
                hfPKey.Value = DecryptString(Request.QueryString["Id"].ToString());
                LoadDataFromDatabase(int.Parse(hfPKey.Value));
                BtnSave.Text = "Update";
            }
            if (Request.QueryString["result"] != null as string)
            {
                string _result = Request.QueryString["result"].ToString();
                if (_result == "save") ShowMessage("Record Saved Successfully", this.Page);
                else if (_result == "update") ShowMessage("Record Updated Successfully", this.Page);
            }
            LoadDataFromDatabase();
        }
    }

    private void LoadDataFromDatabase()
    {
        RoutMasterBL obj = new RoutMasterBL();
        GVDetail.DataSource = obj.LoadAllRoutMasterV();
        GVDetail.DataBind();
    }

    private void LoadDataFromDatabase(int Pkey)
    {
        RoutMasterBL obj = new RoutMasterBL();
        List<DataLayer.RoutMaster> S = obj.LoadRoutMasterByPrimaryKey(Pkey);
        TxtRouteName.Text = S[0].RouteName;
        TxtDescription.Text = S[0].RouteDescription;
        DdnVehicle.SelectedValue = S[0].VehicleId.ToString();
    }

    protected void LnkBtnDelete_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton LnkBtn = (LinkButton)sender;
            GridViewRow gvRow = (GridViewRow)LnkBtn.NamingContainer;
            string Pkey = GVDetail.DataKeys[gvRow.RowIndex].Values[0].ToString();
            int rowIndex = gvRow.RowIndex;
            RoutMasterBL objL = new RoutMasterBL();
            int result = objL.DeleteRoutMaster(int.Parse(Pkey));

            if (result != null)
            {
                ShowMessage("Delete Success", this.Page);
                LoadDataFromDatabase();
            }

        }
        catch (Exception Ex)
        {
            ShowMessage(Ex.InnerException.Message, this.Page);
        }
    }

    protected void LnkBtnEdit_Click(object sender, EventArgs e)
    {
        LinkButton LnkBtn = (LinkButton)sender;
        GridViewRow gvRow = (GridViewRow)LnkBtn.NamingContainer;
        string Pkey = GVDetail.DataKeys[gvRow.RowIndex].Values[0].ToString();
        Response.Redirect("RouteMaster.aspx?Id=" + EncryptString(Pkey));
    }

    protected void BtnSave_Click(object sender, EventArgs e)
    {
        if (!ChkAll()) return;

        if (BtnSave.Text == "Save")
        {
            SaveOrUpdate("Save");
        }
        else
        {
            SaveOrUpdate("Edit");
        }
    }

    private void SaveOrUpdate(string Action)
    {
        try
        {
            RoutMasterBL Obj = new RoutMasterBL();
            if (Action == "Save")
            {
                int _result = Obj.InsertRoutMaster(TxtRouteName.Text, TxtDescription.Text, int.Parse(DdnVehicle.SelectedValue));

                if (_result != null)
                {
                    Response.Redirect("RouteMaster.aspx?result=save", false);
                }
            }
            else
            {
                int _result = Obj.UpdateRoutMaster(int.Parse(hfPKey.Value), TxtRouteName.Text, TxtDescription.Text, int.Parse(DdnVehicle.SelectedValue));
                Response.Redirect("RouteMaster.aspx?result=update", false);

            }
        }

        catch (Exception Ex)
        {
            if (Ex.InnerException.Message.ToUpper().Contains("UK_ROUTNAME"))
            {
                ShowMessage("This Route Already Created.", this.Page);
            }
            else
            {
                ShowMessage(Ex.Message, this.Page);
            }
        }
    }

    private bool ChkAll()
    {
        if (TxtRouteName.Text == "")
        {
            ShowMessage("Enter Route Name", this.Page);
            return false;
        }        
        if (DdnVehicle.SelectedIndex == 0)
        {
            ShowMessage("Select Vehicle.", this.Page);
            return false;
        }


        return true;
    }
}