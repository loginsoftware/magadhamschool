﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login</title>
     <!-- The styles -->
    <link href="css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href="css/charisma-app.css" rel="stylesheet">
    
    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>
<body>
    <form id="form1" runat="server">
   <div class="ch-container">
    <div class="row">
        
    <div class="row">
        <div class="col-md-12 center login-header" style="padding-top: 0px;">
            
            <img src="img/logo-magadham.jpg" width="250px" />
        </div>
        <!--/span-->
    </div><!--/row-->

    <div class="row">
        <div class="well col-md-5 center login-box">
            <div class="alert alert-info">
                Please login with your Username and Password.
            </div>
            <div class="form-horizontal">
                <fieldset>
                <div class="input-group input-group-lg">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user red"></i></span>
                      
       <asp:DropDownList ID="DdnSession" runat="server" Width="100%" Height="54px">
       </asp:DropDownList>
                    </div>
                    <div class="clearfix"></div><br>
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user red"></i></span>
                        <input id="txtUserName" runat="server" type="text" class="form-control" placeholder="Username">
                    </div>
                    <div class="clearfix"></div><br>

                    <div class="input-group input-group-lg">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
                        <input id="txtPassword" runat="server" type="password" class="form-control" placeholder="Password">
                    </div>
                    <div class="clearfix"></div>

                    <%--<div class="input-prepend">
                        <label class="remember" for="remember"><input type="checkbox" id="remember"> Remember me</label>
                    </div>--%>
                    <div class="clearfix"></div>

                    <p class="center col-md-5">
                       
       <asp:Button ID="BtnLogin" runat="server" CssClass="btn btn-primary" Text="Login" 
                            onclick="BtnLogin_Click"></asp:Button>
                    </p>
                </fieldset>
            </div>
        </div>
        <!--/span-->
    </div><!--/row-->
</div><!--/fluid-row-->

</div><!--/.fluid-container-->

<!-- external javascript -->

<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>


    </form>
</body>
</html>
