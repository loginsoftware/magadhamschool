﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;

public partial class ClassMaster : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.QueryString["Id"] != null as string)
            {
                hfPKey.Value = DecryptString(Request.QueryString["Id"].ToString());
                LoadDataFromDatabase(int.Parse(hfPKey.Value));
                BtnSave.Text = "Update";
            }
            if (Request.QueryString["result"] != null as string)
            {
                string _result = Request.QueryString["result"].ToString();
                if (_result == "save") ShowMessage("Record Saved Successfully", this.Page);
                else if (_result == "update") ShowMessage("Record Updated Successfully", this.Page);
            }
            LoadDataFromDatabase();
        }
    }

    private void LoadDataFromDatabase()
    {
        ClassMasterBL obj = new ClassMasterBL();
        GVDetail.DataSource = obj.LoadAllClassMaster();
        GVDetail.DataBind();
    }

    private void LoadDataFromDatabase(int Pkey)
    {
        ClassMasterBL obj = new ClassMasterBL();
        List<DataLayer.ClassMaster> S = obj.LoadClassMasterByPrimaryKey(Pkey);
        TxtClassName.Text = S[0].ClassName;
    }

    protected void LnkBtnDelete_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton LnkBtn = (LinkButton)sender;
            GridViewRow gvRow = (GridViewRow)LnkBtn.NamingContainer;
            string Pkey = GVDetail.DataKeys[gvRow.RowIndex].Values[0].ToString();
            int rowIndex = gvRow.RowIndex;
            ClassMasterBL objL = new ClassMasterBL();
            int result = objL.DeleteClassMaster(int.Parse(Pkey));

            if (result != null)
            {
                ShowMessage("Delete Success", this.Page);
                LoadDataFromDatabase();
            }

        }
        catch (Exception Ex)
        {
            ShowMessage(Ex.InnerException.Message, this.Page);
        }
    }

    protected void LnkBtnEdit_Click(object sender, EventArgs e)
    {
        LinkButton LnkBtn = (LinkButton)sender;
        GridViewRow gvRow = (GridViewRow)LnkBtn.NamingContainer;
        string Pkey = GVDetail.DataKeys[gvRow.RowIndex].Values[0].ToString();
        Response.Redirect("ClassMaster.aspx?Id=" + EncryptString(Pkey));
    }

    protected void BtnSave_Click(object sender, EventArgs e)
    {
        if (!ChkAll()) return;

        if (BtnSave.Text == "Save")
        {
            SaveOrUpdate("Save");
        }
        else
        {
            SaveOrUpdate("Edit");
        }
    }

    private void SaveOrUpdate(string Action)
    {
        try
        {
            ClassMasterBL Obj = new ClassMasterBL();
            if (Action == "Save")
            {
                int _result = Obj.InsertClassMaster(TxtClassName.Text);

                if (_result != null)
                {
                    Response.Redirect("ClassMaster.aspx?result=save", false);
                }
            }
            else
            {
                int _result = Obj.UpdateClassMaster(int.Parse(hfPKey.Value), TxtClassName.Text);
                Response.Redirect("ClassMaster.aspx?result=update", false);

            }
        }

        catch (Exception Ex)
        {
            ShowMessage(Ex.Message, this.Page);
        }
    }

    private bool ChkAll()
    {
        if (TxtClassName.Text == "")
        {
            ShowMessage("Enter Class Name", this.Page);
            return false;
        }

        return true;
    }
}