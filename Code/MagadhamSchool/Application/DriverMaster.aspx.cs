﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;

public partial class DriverMaster : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.QueryString["Id"] != null as string)
            {
                hfPKey.Value = DecryptString(Request.QueryString["Id"].ToString());
                LoadDataFromDatabase(int.Parse(hfPKey.Value));
                BtnSave.Text = "Update";
            }
            if (Request.QueryString["result"] != null as string)
            {
                string _result = Request.QueryString["result"].ToString();
                if (_result == "save") ShowMessage("Record Saved Successfully", this.Page);
                else if (_result == "update") ShowMessage("Record Updated Successfully", this.Page);
            }
            LoadDataFromDatabase();
        }
    }

    private void LoadDataFromDatabase()
    {
        DriverMasterBL obj = new DriverMasterBL();
        GVDetail.DataSource = obj.LoadAllDriverMaster();
        GVDetail.DataBind();
    }

    private void LoadDataFromDatabase(int Pkey)
    {
        DriverMasterBL obj = new DriverMasterBL();
        List<DataLayer.DriverMaster> S = obj.LoadDriverMasterByPrimaryKey(Pkey);
        TxtDriverName.Text = S[0].DriverName;
        TxtFatherName.Text = S[0].DriverFatherName;
        TxtMobileNo.Text = S[0].DriverMobileNo;
        TxtAddress.Text = S[0].DriverAddress;
        TxtUserName.Text = S[0].UserName;
        TxtPassword.Text = S[0].Password;
        chkIsActive.Checked = S[0].IsActive;
    }

    protected void LnkBtnDelete_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton LnkBtn = (LinkButton)sender;
            GridViewRow gvRow = (GridViewRow)LnkBtn.NamingContainer;
            string Pkey = GVDetail.DataKeys[gvRow.RowIndex].Values[0].ToString();
            int rowIndex = gvRow.RowIndex;
            DriverMasterBL objL = new DriverMasterBL();
            int result = objL.DeleteDriverMaster(int.Parse(Pkey));

            if (result != null)
            {
                ShowMessage("Delete Success", this.Page);
                LoadDataFromDatabase();
            }

        }
        catch (Exception Ex)
        {
            ShowMessage(Ex.InnerException.Message, this.Page);
        }
    }

    protected void LnkBtnEdit_Click(object sender, EventArgs e)
    {
        LinkButton LnkBtn = (LinkButton)sender;
        GridViewRow gvRow = (GridViewRow)LnkBtn.NamingContainer;
        string Pkey = GVDetail.DataKeys[gvRow.RowIndex].Values[0].ToString();
        Response.Redirect("DriverMaster.aspx?Id=" + EncryptString(Pkey));
    }

    protected void BtnSave_Click(object sender, EventArgs e)
    {
        if (!ChkAll()) return;

        if (BtnSave.Text == "Save")
        {
            SaveOrUpdate("Save");
        }
        else
        {
            SaveOrUpdate("Edit");
        }
    }

    private void SaveOrUpdate(string Action)
    {
        try
        {
            DriverMasterBL Obj = new DriverMasterBL();
            if (Action == "Save")
            {
                int _result = Obj.InsertDriverMaster(TxtDriverName.Text, TxtFatherName.Text, TxtMobileNo.Text, TxtAddress.Text, chkIsActive.Checked, DateTime.Now, TxtUserName.Text, TxtPassword.Text);

                if (_result != null)
                {
                    Response.Redirect("DriverMaster.aspx?result=save", false);
                }
            }
            else
            {
                int _result = Obj.UpdateDriverMaster(int.Parse(hfPKey.Value), TxtDriverName.Text, TxtFatherName.Text, TxtMobileNo.Text, TxtAddress.Text, chkIsActive.Checked, DateTime.Now, TxtUserName.Text, TxtPassword.Text);
                Response.Redirect("DriverMaster.aspx?result=update", false);

            }
        }

        catch (Exception Ex)
        {
            ShowMessage(Ex.Message, this.Page);
        }
    }

    private bool ChkAll()
    {
        if (TxtDriverName.Text == "")
        {
            ShowMessage("Enter Driver Name", this.Page);
            return false;
        }
        if (TxtMobileNo.Text == "")
        {
            ShowMessage("Enter Mobile No", this.Page);
            return false;
        }
        if (TxtUserName.Text == "")
        {
            ShowMessage("Enter UserName", this.Page);
            return false;
        }
        if (TxtPassword.Text == "")
        {
            ShowMessage("Enter Password", this.Page);
            return false;
        }

        return true;
    }
}