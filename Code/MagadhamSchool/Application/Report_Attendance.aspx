﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Report_Attendance.aspx.cs" Inherits="Report_Attendance" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
    <div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-edit"></i>Attendance Route Wise</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <div role="form">
                    <div class="row form-group">
                        <div class="col-md-4">
                        <label>Date</label>
                            <asp:TextBox ID="TxtDate" runat="server" CssClass="form-control"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender1" TargetControlID="TxtDate" PopupButtonID="TxtDate" Format="dd-MMM-yyyy" runat="server"></asp:CalendarExtender>
                    </div>
                    <div class="col-md-4"> 
                        <label>Vehicle</label><br />
               <asp:DropDownList ID="DdnVehicle" runat="server" AppendDataBoundItems="true" Width="100%" Height="39px" AutoPostBack="True" OnSelectedIndexChanged="DdnVehicle_SelectedIndexChanged">
                 <asp:ListItem>--Select Vehicle--</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-4">
                        <label>Route</label><br />
                 <asp:DropDownList ID="DdnRoute" runat="server" Width="100%" Height="39px" AppendDataBoundItems="true">
                     <asp:ListItem>--Select Route--</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                   <div class="col-md-4">
                        <br />
                       <asp:RadioButtonList ID="rbIsPresent" runat="server" RepeatDirection="Horizontal">
                           <asp:ListItem Selected="True">Present</asp:ListItem>
                           <asp:ListItem>Absent</asp:ListItem>
                           <asp:ListItem>All</asp:ListItem>
                       </asp:RadioButtonList>
                    </div>
                    </div>
                    

                    <div class="row">
                   <div class="col-md-4">
                    <asp:LinkButton ID="BtnShow" runat="server" CssClass="btn btn-default" OnClick="BtnShow_Click">Show</asp:LinkButton>
    <asp:HiddenField ID="hfPKey" runat="server" />
     </div>
     </div>
    
                </div>

            </div>
        </div>
    </div>
    <!--/span-->

</div>

    <div class="row">
        <div class="box col-md-12">
            <div class="box-inner">
                <div class="box-header well" data-original-title="">
                    <h2><i class="glyphicon glyphicon-user"></i> Student Detail</h2>

                    <div class="box-icon">
                        <a href="#" onclick="printdiv('printData');" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-print"></i> Print</a> &nbsp;&nbsp;&nbsp;
                    </div>
                </div>
                <div id="printData" class="box-content">
                    <div align="center" style="font-size:16px;font-weight:bold;">
                        Magadham International School <br />
                        Attendance<br />
                        <asp:Label ID="lblVehicleRoute" runat="server" Text=""></asp:Label>
                    </div>
                                  <asp:GridView ID="GVDetail" AutoGenerateColumns="False" AllowPaging="false" 
            runat="server" DataKeyNames="PickupId" EmptyDataText="No Record Found" 
            Width="100%" CssClass="table table-striped table-bordered responsive">
                            <AlternatingRowStyle BackColor="#ecebeb" />
                            <Columns>
                                 <asp:TemplateField HeaderText="Sr.No" HeaderStyle-Width="10px">
                            <ItemTemplate>
                                <asp:Label ID="lblSRNO" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="AdmissionNo">
                            <ItemTemplate>
                                <asp:Label ID="lblAdmissionNo" runat="server" Text='<%# Eval("AdmissionNo") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Student Name">
                            <ItemTemplate>
                                <asp:Label ID="lblStudentName" runat="server" Text='<%# Eval("StudentName") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>                            
                             <asp:TemplateField HeaderText="Mobile">
                            <ItemTemplate>
                                <asp:Label ID="lblMobileNo" runat="server" Text='<%# Eval("MobileNo") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Address">
                            <ItemTemplate>
                                <asp:Label ID="lblAddress" runat="server" Text='<%# Eval("Address") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Status">
                            <ItemTemplate>
                                <asp:Label ID="lblIsPresent" CssClass='<%# Eval("IsPresent").ToString() == "True" ? "label label-success" : "label label-danger" %>' runat="server" Text='<%# Eval("IsPresent").ToString() == "True" ? "Present": "Absent" %>' />
                            </ItemTemplate>
                            </asp:TemplateField>
                          
                            </Columns>
                            </asp:GridView>


                    
                </div>
            </div>
        </div>
        <!--/span-->

    </div>
    <script language="javascript">
        function printdiv(printpage) {
            var headstr = "<html><head><title></title></head><body>";
            var footstr = "</body>";
            var newstr = document.all.item(printpage).innerHTML;
            var oldstr = document.body.innerHTML;
            document.body.innerHTML = headstr + newstr + footstr;
            window.print();
            document.body.innerHTML = oldstr;
            return false;
        }
</script>
</asp:Content>

