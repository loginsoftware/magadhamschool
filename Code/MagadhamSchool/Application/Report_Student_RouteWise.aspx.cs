﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;

public partial class Report_Student_RouteWise : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            FillVehicle(DdnVehicle);
        }
    }

    protected void BtnShow_Click(object sender, EventArgs e)
    {
        if (DdnVehicle.SelectedIndex > 0 && DdnRoute.SelectedIndex > 0)
        {
            int _sessionId = int.Parse(GetCookie(this.Page, "SessionId"));
            int _vehicleId = int.Parse(DdnVehicle.SelectedValue);
            int _routeId = int.Parse(DdnRoute.SelectedValue);
            StudentMasterBL obj = new StudentMasterBL();
            GVDetail.DataSource = obj.LoadStudentMasterByVehicleIdRouteId(_vehicleId, _routeId, _sessionId);
            GVDetail.DataBind();

        }
    }

    protected void DdnVehicle_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DdnVehicle.SelectedIndex > 0)
        {
            FillRoute(DdnRoute, int.Parse(DdnVehicle.SelectedValue));
        }
    }
}