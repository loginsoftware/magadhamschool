﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="StudentMaster.aspx.cs" Inherits="StudentMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-edit"></i>Student Master</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <div role="form">
                    <div class="row form-group">
                    <div class="col-md-4">
                        <label>Student Name</label>
    <asp:TextBox ID="TxtStudentName" runat="server" CssClass="form-control" placeholder="Enter Student Name"></asp:TextBox>
                    </div>
                    <div class="col-md-4">
                        <label>Father Name</label>
    <asp:TextBox ID="TxtFatherName" runat="server" CssClass="form-control" placeholder="Enter Father Name"></asp:TextBox>
                    </div>
                    <div class="col-md-4">
                        <label>MobileNo</label>
    <asp:TextBox ID="TxtMobileNo" runat="server" CssClass="form-control" placeholder="Enter Mobile Number"></asp:TextBox>
                    </div>
                    <div class="col-md-4">
                        <label>AdmissionNo</label>
    <asp:TextBox ID="TxtAdmissionNo" runat="server" CssClass="form-control" placeholder="Enter AdmissionNo"></asp:TextBox>
                    </div>
                    <div class="col-md-4">
                        <label>Address</label>
    <asp:TextBox ID="TxtAddress" runat="server" CssClass="form-control" placeholder="Enter Address"></asp:TextBox>
                    </div>
                    <div class="col-md-4">
                        <label>Class</label><br />
                 <asp:DropDownList ID="DdnClass" runat="server" AppendDataBoundItems="true" Width="100%" Height="39px">
                 <asp:ListItem>--Select Class--</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-4">
                        <label>Section</label><br />
                 <asp:DropDownList ID="DdnSection" runat="server" AppendDataBoundItems="true" Width="100%" Height="30px">
                 <asp:ListItem>--Select Section--</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-4">
                        <label>Photo</label>
                        <asp:FileUpload ID="FUStudentPhoto"  CssClass="form-control" runat="server" />
                        <asp:Image ID="ImgStudentPhoto" runat="server" Width="50px" Height="70px" />
                    </div>
                    <div class="col-md-4">
                        <label>IsActive</label>
    <asp:CheckBox ID="chkIsActive" runat="server" CssClass="form-control" Checked="true" />
                    </div>
                       
                    </div>
                    <div class="row form-group">
                         <div class="col-md-4">
                        <label>Vehicle Allot</label><br />
                 <asp:DropDownList ID="DdnVehicle" runat="server" AppendDataBoundItems="true" Width="100%" Height="39px" AutoPostBack="True" OnSelectedIndexChanged="DdnVehicle_SelectedIndexChanged">
                 <asp:ListItem>--Select Vehicle--</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-4">
                        <label>Route</label><br />
                 <asp:DropDownList ID="DdnRoute" runat="server" AppendDataBoundItems="true" Width="100%" Height="39px">
                 <asp:ListItem>--Select Route--</asp:ListItem>
                        </asp:DropDownList>
                    </div>

                    </div>

                    <div class="row">
                   <div class="col-md-4">
                    <asp:LinkButton ID="BtnSave" runat="server" CssClass="btn btn-default" OnClick="BtnSave_Click">Save</asp:LinkButton>
    <asp:HiddenField ID="hfStudentPhoto" runat="server" />
    <asp:HiddenField ID="hfAllotmentCode" runat="server" />
    <asp:HiddenField ID="hfDisplayOrder" runat="server" />
    <asp:HiddenField ID="hfAllotmentId" runat="server" />
    <asp:HiddenField ID="hfPKey" runat="server" />
     </div>
     </div>
    
                </div>

            </div>
        </div>
    </div>
    <!--/span-->

</div>

</asp:Content>

