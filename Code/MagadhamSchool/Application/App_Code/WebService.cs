﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using BusinessLayer;
using Newtonsoft.Json;

/// <summary>
/// Summary description for WebService
/// </summary>
[WebService(Namespace = "http://pa.somee.com")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class WebService : System.Web.Services.WebService {

    [WebMethod]
    public string LoginUser(string UserName, string Password)
    {
        string jsonoutput = "";
        DriverMasterBL obj = new DriverMasterBL();
        List<DataLayer.DriverMaster> D = obj.LoadDriverMasterByUserName(UserName);
        if (D.Count > 0)
        {
            if (Password == D[0].Password)
            {
                VehicleMasterBL objS = new VehicleMasterBL();
                List<DataLayer.VehicleMaster> V = objS.LoadVehicleMasterByDriverId(D[0].DriverId);
                jsonoutput = "[{\"Result\":\"Success\",\"Message\":\"Login Successfully\",\"DriverId\":\"" + D[0].DriverId + "\",\"DriverName\":\"" + D[0].DriverName + "\",\"VehicleId\":\"" + V[0].VehicleId + "\",\"VehicleRegNumber\":\"" + V[0].VehicleRegNumber + "\"}]";
            }
            else
            {
                jsonoutput = "[{\"Result\":\"Error\",\"Message\":\"Invalid Password.\"}]";
            }
        }
        else
        {
            jsonoutput = "[{\"Result\":\"Error\",\"Message\":\"Invalid UserName.\"}]";
        }


        return JsonConvert.SerializeObject(jsonoutput); 
    }

    [WebMethod]
    public string GetVehicleRoute(string VehicleId)
    {
        string jsonoutput = "";

        RoutMasterBL objR = new RoutMasterBL();
        List<DataLayer.RoutMaster> R = objR.LoadRoutMasterByVehicleId(int.Parse(VehicleId));

        var json = JsonConvert.SerializeObject(R);

        jsonoutput = "[{\"Result\":\"Success\",\"Message\":"+json+"}]";
       

        return JsonConvert.SerializeObject(jsonoutput);

    }

    [WebMethod]
    public string LoadStudentPickupDrop(string VehicleId, string Type, string RouteId)
    {
        string jsonoutput = "";
        BusAllotmentBL obj = new BusAllotmentBL ();
        SessionMasterBL objS = new SessionMasterBL ();
        List<DataLayer.SessionMaster> S= objS.LoadSessionMasterIsActive();
        if (Type == "Pickup")
        {
            List<DataLayer.VBusAllotment> Dt = obj.LoadBusAllotmentForPickup(int.Parse(VehicleId), int.Parse(RouteId), S[0].SessionId, DateTime.Parse(DateTime.Now.ToString("dd-MMM-yyyy")));
            if (Dt.Count > 0)
            {
                jsonoutput = "[{\"Result\":\"Success\",\"StudentId\":\"" + Dt[0].StudentId + "\",\"StudentName\":\"" + Dt[0].StudentName + "\",\"FatherName\":\"" + Dt[0].FatherName + "\",\"MobileNo\":\"" + Dt[0].MobileNo + "\",\"Class\":\"" + Dt[0].ClassName + "-" + Dt[0].SectionName + "\",\"Address\":\"" + Dt[0].Address + "\",\"StudentPhoto\":\"" + Dt[0].StudentPhoto + "\"}]";
            }
            else
            {
                SchoolReachTxnBL objSR = new SchoolReachTxnBL();
                List<DataLayer.SchoolReachTxn> Sr = objSR.LoadSchoolReachTxnByCreatedOn(int.Parse(VehicleId), int.Parse(RouteId), DateTime.Parse(DateTime.Now.ToString("dd-MMM-yyyy")), "Pickup");
                if (Sr.Count > 0)
                {
                    jsonoutput = "[{\"Result\":\"Error\",\"IsReached\":\"True\",\"Message\":\"You Reached School on: <br> " + Sr[0].ReachDatetime.ToString("dd-MMM-yyyy hh:mm tt") + "\"}]";
                }
                else
                {
                    jsonoutput = "[{\"Result\":\"Error\",\"IsReached\":\"False\",\"Message\":\"Today's Pickup Completed.<br> On School Gate\"}]";
                }
            }
        }
        else if (Type == "Drop")
        {
            List<DataLayer.VBusAllotment> Dt = obj.LoadBusAllotmentForDrop(int.Parse(VehicleId), int.Parse(RouteId), S[0].SessionId, DateTime.Parse(DateTime.Now.ToString("dd-MMM-yyyy")));
            if (Dt.Count > 0)
            {
                jsonoutput = "[{\"Result\":\"Success\",\"StudentId\":\"" + Dt[0].StudentId + "\",\"StudentName\":\"" + Dt[0].StudentName + "\",\"FatherName\":\"" + Dt[0].FatherName + "\",\"MobileNo\":\"" + Dt[0].MobileNo + "\",\"Class\":\"" + Dt[0].ClassName + "-" + Dt[0].SectionName + "\",\"Address\":\"" + Dt[0].Address + "\",\"StudentPhoto\":\"" + Dt[0].StudentPhoto + "\"}]";
            }
            else
            {
                SchoolReachTxnBL objSR = new SchoolReachTxnBL();
                List<DataLayer.SchoolReachTxn> Sr = objSR.LoadSchoolReachTxnByCreatedOn(int.Parse(VehicleId), int.Parse(RouteId), DateTime.Parse(DateTime.Now.ToString("dd-MMM-yyyy")), "Drop");
                if (Sr.Count > 0)
                {
                    jsonoutput = "[{\"Result\":\"Error\",\"IsReached\":\"True\",\"Message\":\"You Reached School on: <br> " + Sr[0].ReachDatetime.ToString("dd-MMM-yyyy hh:mm tt") + "\"}]";
                }
                else
                {
                    jsonoutput = "[{\"Result\":\"Error\",\"IsReached\":\"False\",\"Message\":\"Today's Drop Completed.<br> On School Gate\"}]";
                }
            }

        }

        return JsonConvert.SerializeObject(jsonoutput); 
    }

    [WebMethod]
    public string SavePickupDropTxn(string StudentId, string VehicleId, string IsPresent, string Type, string RouteId)
    {
        string jsonoutput = "";
        if (Type == "Pickup")
        {
            PickupTransactionBL objP = new PickupTransactionBL();
            objP.InsertPickupTransaction(int.Parse(StudentId), int.Parse(VehicleId), "", "", DateTime.Now, bool.Parse(IsPresent), DateTime.Parse(DateTime.Now.ToString("dd-MMM-yyyy")), int.Parse(RouteId));

            jsonoutput = "[{\"Result\":\"Success\",\"Message\":\"Pickup Successfully.\"}]";
        }
        else if (Type == "Drop")
        {
            DropTransactionBL objD = new DropTransactionBL();
            objD.InsertDropTransaction(int.Parse(StudentId), int.Parse(VehicleId), "", "", DateTime.Now, bool.Parse(IsPresent), DateTime.Parse(DateTime.Now.ToString("dd-MMM-yyyy")), int.Parse(RouteId));

            jsonoutput = "[{\"Result\":\"Success\",\"Message\":\"Droped Successfully.\"}]";
        }

        return JsonConvert.SerializeObject(jsonoutput); 

    }

    [WebMethod]
    public string SaveSchoolReachTxn(string VehicleId, string Type, string RouteId)
    {
        string jsonoutput = "";
        if (Type == "Pickup")
        {
            SchoolReachTxnBL objP = new SchoolReachTxnBL();
            objP.InsertSchoolReachTxn(int.Parse(VehicleId), int.Parse(RouteId), DateTime.Now, DateTime.Parse(DateTime.Now.ToString("dd-MMM-yyyy")), "Pickup");

            jsonoutput = "[{\"Result\":\"Success\",\"Message\":\"You Reached School on: <br> " + DateTime.Now.ToString("dd-MMM-yyyy hh:mm tt") + "\"}]";
        }
        else if (Type == "Drop")
        {
            SchoolReachTxnBL objP = new SchoolReachTxnBL();
            objP.InsertSchoolReachTxn(int.Parse(VehicleId), int.Parse(RouteId), DateTime.Now, DateTime.Parse(DateTime.Now.ToString("dd-MMM-yyyy")), "Drop");

            jsonoutput = "[{\"Result\":\"Success\",\"Message\":\"You Reached School on: <br> " + DateTime.Now.ToString("dd-MMM-yyyy hh:mm tt") + "\"}]";
        }

        return JsonConvert.SerializeObject(jsonoutput);

    }
    
}
