﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data;
using System.Configuration;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.IO;
using System.Net;
using System.Threading;
using System.Xml;
using BusinessLayer;
/// <summary>
/// Summary description for UtilityExt
/// </summary>
public partial class Utility 
{
    public string _message = "";
    public void SendMail(string RecipientEmail, string subjectmail, string bodymail, HttpPostedFile postedFile)
    {
        using (MailMessage mm = new MailMessage("pradeep@solyn.in", RecipientEmail.Trim()))
        {
            mm.Subject = subjectmail;
            mm.Body = bodymail;
            //if (postedFile.ContentLength > 0)
            //{
            //    string fileName = Path.GetFileName(postedFile.FileName);
            //    mm.Attachments.Add(new Attachment(postedFile.InputStream, fileName));
            //}
            mm.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.EnableSsl = true;
            NetworkCredential NetworkCred = new NetworkCredential("pradeep@solyn.in", "vidisha@0755");
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = NetworkCred;
            smtp.Port = 587;
            smtp.Send(mm);
        }
    }

    public string SendSmsToParent(string SenderMobileNo, string StudentName, string DelayMessage)
    {
        string sMessage = "Dear " + StudentName + ", " + DelayMessage + ", Magadham International School.";
        string sURL = "http://cloud.smsindiahub.in/vendorsms/pushsms.aspx?user=pradeeplodhi&password=pradeep91&msisdn=" + SenderMobileNo + "&sid=MAGADM&msg=" + sMessage + "&fl=0&gwid=2";

        string _resp = GetResponse(sURL);
        return _resp;
    }

    public static string GetResponse(string sURL)
    {
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(sURL);
        request.MaximumAutomaticRedirections = 4;
        request.Credentials = CredentialCache.DefaultCredentials;
        try
        {
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream receiveStream = response.GetResponseStream();
            StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
            string sResponse = readStream.ReadToEnd();
            response.Close();
            readStream.Close();
            return sResponse;
        }
        catch
        {
            return "";
        }
    }

    public void FillSession(DropDownList ControlName)
    {
        SessionMasterBL obj = new SessionMasterBL();
        ControlName.DataSource = obj.LoadAllSessionMaster();
        ControlName.DataTextField = "SessionName";
        ControlName.DataValueField = "SessionId";
        ControlName.DataBind();
    }
    public void FillDriver(DropDownList ControlName)
    {
        DriverMasterBL obj = new DriverMasterBL();
        ControlName.DataSource = obj.LoadAllDriverMaster();
        ControlName.DataTextField = "DriverName";
        ControlName.DataValueField = "DriverId";
        ControlName.DataBind();
    }
    public void FillClass(DropDownList ControlName)
    {
        ClassMasterBL obj = new ClassMasterBL();
        ControlName.DataSource = obj.LoadAllClassMaster();
        ControlName.DataTextField = "ClassName";
        ControlName.DataValueField = "ClassId";
        ControlName.DataBind();
    }
    public void FillSection(DropDownList ControlName)
    {
        SectionMasterBL obj = new SectionMasterBL();
        ControlName.DataSource = obj.LoadAllSectionMaster();
        ControlName.DataTextField = "SectionName";
        ControlName.DataValueField = "SectionId";
        ControlName.DataBind();
    }
    public void FillVehicle(DropDownList ControlName)
    {
        VehicleMasterBL obj = new VehicleMasterBL();
        ControlName.DataSource = obj.LoadVehicleMasterVehicle();
        ControlName.DataTextField = "VehicleCode";
        ControlName.DataValueField = "VehicleId";
        ControlName.DataBind();
    }
    public void FillRoute(DropDownList ControlName, int VehicleId)
    {
        ControlName.Items.Clear();
        ControlName.Items.Add("--Select Route--");
        RoutMasterBL obj = new RoutMasterBL();
        ControlName.DataSource = obj.LoadRoutMasterByVehicleId(VehicleId);
        ControlName.DataTextField = "RouteName";
        ControlName.DataValueField = "RouteId";
        ControlName.DataBind();
    }
    public void FillRoute(DropDownList ControlName)
    {
        RoutMasterBL obj = new RoutMasterBL();
        ControlName.DataSource = obj.LoadAllRoutMaster();
        ControlName.DataTextField = "RouteName";
        ControlName.DataValueField = "RouteId";
        ControlName.DataBind();
    }
    public void FillShift(DropDownList ControlName)
    {
        ShiftMasterBL obj = new ShiftMasterBL();
        ControlName.DataSource = obj.LoadAllShiftMaster();
        ControlName.DataTextField = "ShiftName";
        ControlName.DataValueField = "ShiftId";
        ControlName.DataBind();
    }
    public void FillShiftRadioButton(RadioButtonList ControlName)
    {
        ShiftMasterBL obj = new ShiftMasterBL();
        ControlName.DataSource = obj.LoadAllShiftMaster();
        ControlName.DataTextField = "ShiftName";
        ControlName.DataValueField = "ShiftId";
        ControlName.DataBind();
    }

    //public void FillColony(DropDownList ControlName)
    //{
    //    ColonyMasterBL obj = new ColonyMasterBL();
    //    ControlName.DataSource = obj.LoadAllColonyMaster();
    //    ControlName.DataTextField = "ColonyName";
    //    ControlName.DataValueField = "ColonyId";
    //    ControlName.DataBind();
    //}

    public string GenerateRandomString()
    {
        string otpa = "";
        string PasswordLength = "12";
        // string NewPassword = "";
        string allowedChars = "";
        allowedChars = "1,2,3,4,5,6,7,8,9,0";
        allowedChars += "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,";
        allowedChars += "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,";
        //allowedChars += "~,!,@,#,$,%,^,&,*,+,?";
        //Then working with an array...
        char[] sep = { ',' };
        string[] arr = allowedChars.Split(sep);
        string IDString = "";
        string temp = "";
        //utilize the "random" class
        Random rand = new Random();
        //and lastly - loop through the generation process...
        for (int i = 0; i < Convert.ToInt32(PasswordLength); i++)
        {
            temp = arr[rand.Next(0, arr.Length)];
            IDString += temp;
            otpa = IDString;

            //For Testing purposes, I used a label on the front end to show me the generated password.
            // fullname.Text = IDString;

        }

        return otpa;
    }
    
    public DataTable ToDataTable<T>(List<T> items)
    {

        DataTable dataTable = new DataTable(typeof(T).Name);

        //Get all the properties

        PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);

        foreach (PropertyInfo prop in Props)
        {

            //Setting column names as Property names

            dataTable.Columns.Add(prop.Name);

        }

        foreach (T item in items)
        {

            var values = new object[Props.Length];

            for (int i = 0; i < Props.Length; i++)
            {

                //inserting property values to datatable rows

                values[i] = Props[i].GetValue(item, null);

            }

            dataTable.Rows.Add(values);

        }

        //put a breakpoint here and check datatable

        return dataTable;

    }

}