﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;

public partial class StudentMasterDetail : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            FillVehicle(DdnVehicle);
            LoadDataFromDatabase();
        }
    }

    protected void DdnVehicle_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DdnVehicle.SelectedIndex > 0)
        {
            FillRoute(DdnRoute, int.Parse(DdnVehicle.SelectedValue));
        }
    }

    protected void BtnShow_Click(object sender, EventArgs e)
    {

        LoadDataFromDatabase();
    }

    private void LoadDataFromDatabase()
    {
        int _vehicleId = 0;
        int _routeId = 0;
        int _sessionId = int.Parse(GetCookie(this.Page, "SessionId"));
        if(DdnVehicle.SelectedIndex > 0) _vehicleId = int.Parse(DdnVehicle.SelectedValue);
        if(DdnRoute.SelectedIndex > 0) _routeId = int.Parse(DdnRoute.SelectedValue);
        StudentMasterBL obj = new StudentMasterBL();
        GVDetail.DataSource = obj.LoadStudentMasterByVehicleIdRouteId(_vehicleId, _routeId, _sessionId);
        GVDetail.DataBind();
    }

    protected void LnkBtnDelete_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton LnkBtn = (LinkButton)sender;
            GridViewRow gvRow = (GridViewRow)LnkBtn.NamingContainer;
            string _studentId = GVDetail.DataKeys[gvRow.RowIndex].Values[0].ToString();
            string _allotmentId = GVDetail.DataKeys[gvRow.RowIndex].Values[1].ToString();
            int rowIndex = gvRow.RowIndex;
            StudentMasterBL objL = new StudentMasterBL();
            int result = objL.DeleteStudentMasterAndBusAllotment(int.Parse(_studentId), int.Parse(_allotmentId));

            if (result != null)
            {
                ShowMessage("Delete Success", this.Page);
                LoadDataFromDatabase();
            }

        }
        catch (Exception Ex)
        {
            ShowMessage(Ex.InnerException.Message, this.Page);
        }
    }

    protected void LnkBtnEdit_Click(object sender, EventArgs e)
    {
        LinkButton LnkBtn = (LinkButton)sender;
        GridViewRow gvRow = (GridViewRow)LnkBtn.NamingContainer;
        string studentId = GVDetail.DataKeys[gvRow.RowIndex].Values[0].ToString();
        string allotmentId = GVDetail.DataKeys[gvRow.RowIndex].Values[1].ToString();
        Response.Redirect("StudentMaster.aspx?Id=" + EncryptString(studentId) + "&allotmentid=" + EncryptString(allotmentId));
    }


}