﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;

public partial class VehicleMaster : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            FillDriver(DdnDriver);
            if (Request.QueryString["Id"] != null as string)
            {
                hfPKey.Value = DecryptString(Request.QueryString["Id"].ToString());
                LoadDataFromDatabase(int.Parse(hfPKey.Value));
                BtnSave.Text = "Update";
            }
            if (Request.QueryString["result"] != null as string)
            {
                string _result = Request.QueryString["result"].ToString();
                if (_result == "save") ShowMessage("Record Saved Successfully", this.Page);
                else if (_result == "update") ShowMessage("Record Updated Successfully", this.Page);
            }
            LoadDataFromDatabase();
        }
    }

    private void LoadDataFromDatabase()
    {
        VehicleMasterBL obj = new VehicleMasterBL();
        GVDetail.DataSource = obj.LoadAllVehicleMasterV();
        GVDetail.DataBind();
    }

    private void LoadDataFromDatabase(int Pkey)
    {
        VehicleMasterBL obj = new VehicleMasterBL();
        List<DataLayer.VehicleMaster> S = obj.LoadVehicleMasterByPrimaryKey(Pkey);
        TxtVehicleCode.Text = S[0].VehicleCode;
        DdnVehicleType.SelectedValue = S[0].VehicleType;
        TxtVehicleRegNumber.Text = S[0].VehicleRegNumber;
        TxtTotalSeat.Text = S[0].TotalSeat;
        DdnDriver.SelectedValue = S[0].DriverId.ToString();
        chkIsActive.Checked = S[0].IsActive;
    }

    protected void LnkBtnDelete_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton LnkBtn = (LinkButton)sender;
            GridViewRow gvRow = (GridViewRow)LnkBtn.NamingContainer;
            string Pkey = GVDetail.DataKeys[gvRow.RowIndex].Values[0].ToString();
            int rowIndex = gvRow.RowIndex;
            VehicleMasterBL objL = new VehicleMasterBL();
            int result = objL.DeleteVehicleMaster(int.Parse(Pkey));

            if (result != null)
            {
                ShowMessage("Delete Success", this.Page);
                LoadDataFromDatabase();
            }

        }
        catch (Exception Ex)
        {
            ShowMessage(Ex.InnerException.Message, this.Page);
        }
    }

    protected void LnkBtnEdit_Click(object sender, EventArgs e)
    {
        LinkButton LnkBtn = (LinkButton)sender;
        GridViewRow gvRow = (GridViewRow)LnkBtn.NamingContainer;
        string Pkey = GVDetail.DataKeys[gvRow.RowIndex].Values[0].ToString();
        Response.Redirect("VehicleMaster.aspx?Id=" + EncryptString(Pkey));
    }

    protected void BtnSave_Click(object sender, EventArgs e)
    {
        if (!ChkAll()) return;

        if (BtnSave.Text == "Save")
        {
            SaveOrUpdate("Save");
        }
        else
        {
            SaveOrUpdate("Edit");
        }
    }

    private void SaveOrUpdate(string Action)
    {
        try
        {
            VehicleMasterBL Obj = new VehicleMasterBL();
            if (Action == "Save")
            {
                int _result = Obj.InsertVehicleMaster(TxtVehicleCode.Text.ToUpper(), DdnVehicleType.SelectedValue, TxtVehicleRegNumber.Text.ToUpper(), TxtTotalSeat.Text, chkIsActive.Checked, int.Parse(DdnDriver.SelectedValue));

                if (_result != null)
                {
                    Response.Redirect("VehicleMaster.aspx?result=save", false);
                }
            }
            else
            {
                int _result = Obj.UpdateVehicleMaster(int.Parse(hfPKey.Value), TxtVehicleCode.Text.ToUpper(), DdnVehicleType.SelectedValue, TxtVehicleRegNumber.Text.ToUpper(), TxtTotalSeat.Text, chkIsActive.Checked, int.Parse(DdnDriver.SelectedValue));
                Response.Redirect("VehicleMaster.aspx?result=update", false);

            }
        }

        catch (Exception Ex)
        {
            if (Ex.InnerException.Message.ToUpper().Contains("UK_DRIVERID"))
            {
                ShowMessage("This Driver Already Asigned.", this.Page);
            }
            else
            {
                ShowMessage(Ex.Message, this.Page);
            }
        }
    }

    private bool ChkAll()
    {
        if (TxtVehicleCode.Text == "")
        {
            ShowMessage("Enter Vehicle Code", this.Page);
            return false;
        }
        if (TxtVehicleRegNumber.Text == "")
        {
            ShowMessage("Enter Registration No", this.Page);
            return false;
        }
        if (DdnDriver.SelectedIndex == 0)
        {
            ShowMessage("Select Vehicle Driver.", this.Page);
            return false;
        }
       
        
        return true;
    }
}