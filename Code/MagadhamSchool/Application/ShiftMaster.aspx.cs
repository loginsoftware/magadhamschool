﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;

public partial class ShiftMaster : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.QueryString["Id"] != null as string)
            {
                hfPKey.Value = DecryptString(Request.QueryString["Id"].ToString());
                LoadDataFromDatabase(int.Parse(hfPKey.Value));
                BtnSave.Text = "Update";
            }
            if (Request.QueryString["result"] != null as string)
            {
                string _result = Request.QueryString["result"].ToString();
                if (_result == "save") ShowMessage("Record Saved Successfully", this.Page);
                else if (_result == "update") ShowMessage("Record Updated Successfully", this.Page);
            }
            LoadDataFromDatabase();
        }
    }

    private void LoadDataFromDatabase()
    {
        ShiftMasterBL obj = new ShiftMasterBL();
        GVDetail.DataSource = obj.LoadAllShiftMaster();
        GVDetail.DataBind();
    }

    private void LoadDataFromDatabase(int Pkey)
    {
        ShiftMasterBL obj = new ShiftMasterBL();
        List<DataLayer.ShiftMaster> S = obj.LoadShiftMasterByPrimaryKey(Pkey);
        TxtShiftName.Text = S[0].ShiftName;
    }

    protected void LnkBtnDelete_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton LnkBtn = (LinkButton)sender;
            GridViewRow gvRow = (GridViewRow)LnkBtn.NamingContainer;
            string Pkey = GVDetail.DataKeys[gvRow.RowIndex].Values[0].ToString();
            int rowIndex = gvRow.RowIndex;
            ShiftMasterBL objL = new ShiftMasterBL();
            int result = objL.DeleteShiftMaster(int.Parse(Pkey));

            if (result != null)
            {
                ShowMessage("Delete Success", this.Page);
                LoadDataFromDatabase();
            }

        }
        catch (Exception Ex)
        {
            ShowMessage(Ex.InnerException.Message, this.Page);
        }
    }

    protected void LnkBtnEdit_Click(object sender, EventArgs e)
    {
        LinkButton LnkBtn = (LinkButton)sender;
        GridViewRow gvRow = (GridViewRow)LnkBtn.NamingContainer;
        string Pkey = GVDetail.DataKeys[gvRow.RowIndex].Values[0].ToString();
        Response.Redirect("ShiftMaster.aspx?Id=" + EncryptString(Pkey));
    }

    protected void BtnSave_Click(object sender, EventArgs e)
    {
        if (!ChkAll()) return;

        if (BtnSave.Text == "Save")
        {
            SaveOrUpdate("Save");
        }
        else
        {
            SaveOrUpdate("Edit");
        }
    }

    private void SaveOrUpdate(string Action)
    {
        try
        {
            ShiftMasterBL Obj = new ShiftMasterBL();
            if (Action == "Save")
            {
                int _result = Obj.InsertShiftMaster(TxtShiftName.Text);

                if (_result != null)
                {
                    Response.Redirect("ShiftMaster.aspx?result=save", false);
                }
            }
            else
            {
                int _result = Obj.UpdateShiftMaster(int.Parse(hfPKey.Value), TxtShiftName.Text);
                Response.Redirect("ShiftMaster.aspx?result=update", false);

            }
        }

        catch (Exception Ex)
        {
            ShowMessage("Shift Already Created", this.Page);
        }
    }

    private bool ChkAll()
    {
        if (TxtShiftName.Text == "")
        {
            ShowMessage("Enter Shift Name", this.Page);
            return false;
        }

        return true;
    }
}