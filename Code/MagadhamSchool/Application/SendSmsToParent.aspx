﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="SendSmsToParent.aspx.cs" Inherits="SendSmsToParent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-edit"></i>Send Sms to Parents</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <div role="form">
                    <div class="form-group">
                        <label>Vehicle</label>
                        <asp:DropDownList ID="DdnVehicle" CssClass="form-control" runat="server" AppendDataBoundItems="true">
                        <asp:ListItem>--Select Vehicle--</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label>Delay Time</label>
                        <asp:TextBox ID="TxtDelayTime" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                  
    <asp:LinkButton ID="BtnSend" runat="server" CssClass="btn btn-default" OnClick="BtnSend_Click">Send SMS</asp:LinkButton>
    <asp:HiddenField ID="hfPKey" runat="server" />
                </div>

            </div>
        </div>
    </div>
    <!--/span-->

</div>
</asp:Content>

