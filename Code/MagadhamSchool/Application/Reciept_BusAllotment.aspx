﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Reciept_BusAllotment.aspx.cs" Inherits="Reciept_BusAllotment" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="border:1px solid black;padding:3px 3px 3px 3px;">    
        <table style="width:100%;">
            <tr><td style="text-align:center;font-weight:bold;font-size:26px;">Magadham International School</td></tr>
            <tr><td style="text-align:center;font-weight:bold;font-size:16px;">Address: Plod Compound, Vitthal Nagar, Near Idgah Tiraha Vidisha</td></tr>
            <tr><td style="text-align:center;font-weight:bold;font-size:16px;">Phone: +91-91099-19401 , +91-91099-18402 </td></tr>
            <tr><td style="text-align:center;font-weight:bold;font-size:16px;">Web: www.magadham.in , Email: magadham.info@gmail.com </td></tr>
        </table>

        <table style="width:100%">
             <tr><td>Reciept Date: <asp:Label ID="lblRecieptDate1" runat="server" Text=""></asp:Label></td> <td style="float:right;">Session: <asp:Label ID="lblSessionName1" runat="server" Text=""></asp:Label></td></tr>
        </table>
        <div align="center">
        <table style="width:60%;">           
            <tr><td>Token No.</td><td><asp:Label ID="lblAllotmentCode1" runat="server" Text=""></asp:Label></td></tr>
            <tr><td>Student Name</td><td><asp:Label ID="lblStudentName1" runat="server" Text=""></asp:Label></td></tr>
            <tr><td>Father Name</td><td><asp:Label ID="lblFatherName1" runat="server" Text=""></asp:Label></td></tr>
            <tr><td>Mobile No.</td><td><asp:Label ID="lblMobileNo1" runat="server" Text=""></asp:Label></td></tr>
            <tr><td>Address</td><td><asp:Label ID="lblAddress1" runat="server" Text=""></asp:Label></td></tr>
        </table>
        </div>
         <table style="margin-top:20px;">
             <tr><td>Driver: </td> <td><asp:Label ID="lblDriverName1" runat="server" Text=""></asp:Label></td><td>MobileNo:</td><td><asp:Label ID="lblDriverMobile1" runat="server" Text=""></asp:Label></td></tr>
             <tr><td>Van/Bus No: </td> <td><asp:Label ID="lblVehicleRegNo1" runat="server" Text=""></asp:Label></td><td>Route:</td><td><asp:Label ID="lblRouteName1" runat="server" Text=""></asp:Label></td></tr>
        </table>
        <br /><br /><br />
         <div align="center" style="font-size:20px;font-weight:bold;">Thank You</div>
    </div>


<hr style="border: 2px dashed #000; border-style: none none dashed; color: #000; background-color: #fff;margin-top:50px;margin-bottom:50px;"/>

        <div style="border:1px solid black;padding:3px 3px 3px 3px;">    
        <table style="width:100%;">
            <tr><td style="text-align:center;font-weight:bold;font-size:26px;">Magadham International School</td></tr>
            <tr><td style="text-align:center;font-weight:bold;font-size:16px;">Address: Plod Compound, Vitthal Nagar, Near Idgah Tiraha Vidisha</td></tr>
            <tr><td style="text-align:center;font-weight:bold;font-size:16px;">Phone: +91-91099-19401 , +91-91099-18402 </td></tr>
            <tr><td style="text-align:center;font-weight:bold;font-size:16px;">Web: www.magadham.in , Email: magadham.info@gmail.com </td></tr>
        </table>

        <table style="width:100%">
             <tr><td>Reciept Date: <asp:Label ID="lblRecieptDate" runat="server" Text=""></asp:Label></td> <td style="float:right;">Session: <asp:Label ID="lblSessionName" runat="server" Text=""></asp:Label></td></tr>
        </table>
        <div align="center">
        <table style="width:60%;">           
            <tr><td>Token No.</td><td><asp:Label ID="lblAllotmentCode" runat="server" Text=""></asp:Label></td></tr>
            <tr><td>Student Name</td><td><asp:Label ID="lblStudentName" runat="server" Text=""></asp:Label></td></tr>
            <tr><td>Father Name</td><td><asp:Label ID="lblFatherName" runat="server" Text=""></asp:Label></td></tr>
            <tr><td>Mobile No.</td><td><asp:Label ID="lblMobileNo" runat="server" Text=""></asp:Label></td></tr>
            <tr><td>Address</td><td><asp:Label ID="lblAddress" runat="server" Text=""></asp:Label></td></tr>
        </table>
        </div>
         <table style="margin-top:20px;">
             <tr><td>Driver: </td> <td><asp:Label ID="lblDriverName" runat="server" Text=""></asp:Label></td><td>MobileNo:</td><td><asp:Label ID="lblDriverMobile" runat="server" Text=""></asp:Label></td></tr>
             <tr><td>Van/Bus No: </td> <td><asp:Label ID="lblVehicleRegNo" runat="server" Text=""></asp:Label></td><td>Route:</td><td><asp:Label ID="lblRouteName" runat="server" Text=""></asp:Label></td></tr>
        </table>
        <br /><br /><br />
         <div align="center" style="font-size:20px;font-weight:bold;">Thank You</div>
    </div>
    </form>
     <script type="text/javascript">
         function printPage() {
             window.print();
         }
         printPage();
    </script>
</body>
</html>
